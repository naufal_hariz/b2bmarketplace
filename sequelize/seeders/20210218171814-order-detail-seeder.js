"use strict";
const { v4: uuidv4 } = require("uuid");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const orderDetails = [
      {
        id: uuidv4(),
        productId: 2,
        orderId: "705a2574-1802-4605-92d4-b23e027b26a2",
        quantity: 1,
        price: 300000,
        tax: 0,
        discount: 0,
        total: 300000,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: uuidv4(),
        productId: 3,
        orderId: "705a2574-1802-4605-92d4-b23e027b26a2",
        quantity: 1,
        price: 366664,
        tax: 0,
        discount: 0,
        total: 366664,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
    ];
    await queryInterface.bulkInsert("OrderDetails", orderDetails);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
