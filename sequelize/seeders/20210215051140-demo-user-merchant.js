'use strict';
const { v4: uuidv4 } = require("uuid");
/**
 * USER DEMO MERCHANT
 *
 * "email": "merchant@tobiko.com",
 * "password": "merchant123", 
 * 
 */

module.exports = {
    up: async (queryInterface, Sequelize) => {
        let merchant = [{
            id: uuidv4(),
            name: "Merchant Default",
            phone: "0811878789",
            address: "Jakarta",
            provinceId: 21,
            cityId: 2,
            latitude: -6.200000,
            longitude: 106.816666,
            photo: "https://dzahin-storage.s3-ap-southeast-1.amazonaws.com/tobiko/merchant/logo-kios.jpg",
            adminMerchantId: "8aad1208-2155-4cb5-8b0e-7af688d6af6f",
            verificationStatus: "verified",
            verificationTime: "2021-02-15 05:18:22.858+00",
            verificationBy: "Admin",
            cashBalance: 1000000,
            verificationNotes: ""
        }]
        await queryInterface.bulkInsert("Merchants", merchant);
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('Merchants', null, {});
    }
};
