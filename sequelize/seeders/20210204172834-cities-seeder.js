"use strict";
const RajaOngkirRepository = require("../../src/repository/rajaongkir.repository");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const rajaOngkirRepository = new RajaOngkirRepository();
    const response = await rajaOngkirRepository.getListCitiesFromRajaOngkir();
    const cities = response.results.map((data) => {
      return {
        id: data.city_id,
        provinceId: data.province_id,
        name: data.city_name,
        type: data.type,
        postalCode: data.postal_code,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      };
    });
    await queryInterface.bulkInsert("Cities", cities);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
