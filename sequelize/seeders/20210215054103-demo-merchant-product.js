'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        let merchantProduct = [
            {
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                productId: 1
            },
            {
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                productId: 2
            },
            {
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                productId: 3
            },
            {
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                productId: 4
            },
            {
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                productId: 5
            }
        ]
        await queryInterface.bulkInsert("MerchantProducts", merchantProduct);
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('MerchantProducts', null, {});
    }
};
