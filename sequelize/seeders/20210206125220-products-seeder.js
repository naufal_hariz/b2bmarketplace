"use strict";
const productB2Bs = require("../../src/repository/product-b2b-tsel.json");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const products = productB2Bs.map((product) => {
      return {
        id: product.id,
        name: product.name,
        description: product.description,
        feature: product.feature,
        banner: product.banner,
        target: product.target,
        categoryId: product.categoryId,
        link: product.link,
        weight: Math.floor(Math.random() * 2000),
        price: Math.floor(Math.random() * 10 + 1) * 10000,
        isNew: Math.random() > 0.5,
        isPopular: Math.random() > 0.5,
        isRecommended: Math.random() > 0.5,
        isHot: Math.random() > 0.5,
        isFavourite: Math.random() > 0.5,
        isTop: Math.random() > 0.5,
        countRating: Math.floor(Math.random() * 100) + 1,
        averageRating: Math.floor(Math.random() * 500 + 500) / 100,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      };
    });
    await queryInterface.bulkInsert("Products", products);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
