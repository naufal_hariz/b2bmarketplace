'use strict';

const { v4: uuidv4 } = require("uuid");

module.exports = {
    up: async (queryInterface, Sequelize) => {
        let ids = [
            uuidv4(), uuidv4(), uuidv4()
        ];

        console.log(ids)

        let order = [
            {
                id: ids[0],
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                customerId: "89cedb5d-08b1-45c1-80c5-35c2b7cc2713",
                paymentTime: new Date(),
                paymentMethod: "manual_transfer",
                paymentTo: "Mandiri 144 444 444444",
                paymentNominal: 159000,
                paymentProof: "https://dzahin-storage.s3-ap-southeast-1.amazonaws.com/tobiko/others/DqaB4FYU0AAN4xV.jpg",
                submittedTime: new Date(),
                invoiceNumber: "INV001"
            },
            {
                id: ids[1],
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                customerId: "89cedb5d-08b1-45c1-80c5-35c2b7cc2713",
                paymentTime: new Date(),
                paymentMethod: "manual_transfer",
                paymentTo: "Mandiri 144 444 444444",
                paymentNominal: 150000,
                paymentProof: "https://dzahin-storage.s3-ap-southeast-1.amazonaws.com/tobiko/others/DqaB4FYU0AAN4xV.jpg",
                submittedTime: new Date(),
                invoiceNumber: "INV002"
            },
            {
                id: ids[2],
                merchantId: "34554e08-bd38-440e-a180-da4f7ddc337e",
                customerId: "89cedb5d-08b1-45c1-80c5-35c2b7cc2713",
                paymentTime: new Date(),
                paymentMethod: "manual_transfer",
                paymentTo: "Mandiri 144 444 444444",
                paymentNominal: 700000,
                paymentProof: "https://dzahin-storage.s3-ap-southeast-1.amazonaws.com/tobiko/others/DqaB4FYU0AAN4xV.jpg",
                submittedTime: new Date(),
                invoiceNumber: "INV003"
            },
        ]

        let orderDetail = [
            {
                id: uuidv4(),
                productId: 1,
                orderId: ids[0],
                quantity: 1,
                price: 50000,
                tax: 5000,
                discount: 0,
                total: 55000
            },
            {
                id: uuidv4(),
                productId: 2,
                orderId: ids[0],
                quantity: 1,
                price: 100000,
                tax: 0,
                discount: 0,
                total: 100000
            },
            {
                id: uuidv4(),
                productId: 3,
                orderId: ids[0],
                quantity: 1,
                price: 40000,
                tax: 0,
                discount: 0,
                total: 4000
            },
            {
                id: uuidv4(),
                productId: 4,
                orderId: ids[1],
                quantity: 3,
                price: 50000,
                tax: 0,
                discount: 0,
                total: 150000
            },
            {
                id: uuidv4(),
                productId: 5,
                orderId: ids[2],
                quantity: 10,
                price: 700000,
                tax: 0,
                discount: 0,
                total: 0
            },
        ]

        await queryInterface.bulkInsert("Orders", order);
        await queryInterface.bulkInsert("OrderDetails", orderDetail);
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('Orders', null, {});
        await queryInterface.bulkDelete('OrderDetails', null, {});
    }
};
