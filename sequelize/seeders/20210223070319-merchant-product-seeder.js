"use strict";
const productB2Bs = require("../../src/repository/product-b2b-tsel.json");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const merchantId = "93f2e033-50ee-4b7c-a851-3f4d78847b25";
    const products = productB2Bs.map((product, index) => {
      return {
        id: index + 1,
        merchantId,
        productId: product.id,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      };
    });
    await queryInterface.bulkInsert("MerchantProducts", products);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
