'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const news = [
    {
      id: 1,
      title: "Bayar Pakai AKULAKU, Diskon Hingga Rp100 ribuan!",
      subtitle: "Makin Untung Beli Modem Telkomsel Orbit",
      description: "Anda memiliki kesempatan untuk mendapatkan diskon sebesar 5% untuk pembelian modem dengan menggunakan AKULAKU sebagai metode pembayaran. Diskon akan langsung mengurangi nominal transaksi apabila kode promo AKULAKUMD dimasukkan ke dalam halaman pembayaran Akulaku. \n Ayo beli modem Orbit dengan kualitas jaringan Telkomsel 4G LTE yang stabil sekarang juga! Instalasi mudah tanpa ribet. Kini sudah tersedia layanan same day untuk pengiriman modem Orbit dan gratis ongkos kirim hingga 20 ribu. \n Jangan lupa juga isi paket data Orbit karena paket data Orbit masih diskon 50%. Anda juga bisa mendapatkan tambahan paket data 5 GB jika menggunakan kode referal teman Anda yang sudah menggunakan Orbit. Dapatkan juga berbagai promo menarik lainnya di website Orbit myorbit.id.",
      tnc: "Promo merupakan diskon sebesar 5% untuk pembelian modem Orbit \n Promo hanya berlaku untuk pembayaran melalui AKULAKU \n Promo berlaku pada periode 19-28 Februari 2021 \n Promo berlaku 1x untuk pengguna Akulaku/periode promo",
      isPublished: true,
      isFeatured: false
    },
    {
      id: 2,
      title: "Cashback 50% Pembayaran LinkAja",
      subtitle: "Beli Apa Aja di Telkomsel Orbit, Dapat Cashback LinkAja. Jadi Beli Sekarang!	",
      description: "Anda memiliki kesempatan untuk mendapatkan cashback 50% maksimum sebesar Rp10.000 untuk pembelian modem atau semua jenis paket data Orbit dan melakukan pembayaran dengan LinkAja. Cashback akan diberikan dalam bentuk saldo bonus LinkAja ke nomor LinkAja pelanggan/MSISDN yang digunakan untuk melakukan pembayaran. \n Ayo beli modem Orbit dengan kualitas jaringan Telkomsel 4G LTE yang stabil sekarang juga! Instalasi mudah tanpa ribet. Kini sudah tersedia layanan same day untuk pengiriman modem Orbit dan gratis ongkos kirim hingga 20 ribu. \n Jangan lupa juga isi paket data Orbit karena paket data Orbit masih diskon 50%. Anda juga bisa mendapatkan tambahan paket data 5 GB jika menggunakan kode referal teman Anda yang sudah menggunakan Orbit. Dapatkan juga berbagai promo menarik lainnya di website Orbit myorbit.id.",
      tnc: "Promo merupakan cashback sebesar 50% untuk pembelian modem Orbit atau paket data Orbit \n Promo berlaku untuk pembelian modem Orbit jenis apapun \n Promo berlaku untuk pembelian semua jenis paket data \n Promo hanya berlaku untuk pembayaran melalui LinkAja \n Promo berlaku untuk nilai transaksi minimum Rp20.000 \n Maksimum cashback yang akan diberikan sebesar Rp10.000",
      isPublished: true,
      isFeatured: true
    },
    {
      id: 3,
      title: "#DiRumahTerusProduktif dengan CloudX Meeting on Demandbit",
      subtitle: "Layanan ViCon dengan 100 peserta",
      description: "Meeting di mana pun dan kapan pun lebih mudah dengan CloudX Meeting on Demand. Dapatkan berbagai durasi meeting hingga 5 jam mulai dari Rp10.000.",
      tnc: "Pastikan Anda sudah mendownload aplikasi CloudX di smartphone (Android atau iOS). Sedangkan untuk pengguna desktop, pelanggan hanya perlu menginstal plugin CloudX (setelah link di click kemudian pilih “Download & Run”, setelah download selesai – Open Meeting).\nCloudX Meeting dapat diakses menggunakan koneksi internet 4G, WiFi, atau internet kabel.\nBeli paket CloudX Meeting untuk mendapatkan link meeting dan password.\nShare atau bagikan link tersebut ke peserta yang akan diundang meeting. Pastikan peserta sudah mendownload aplikasi CloudX.\nPeserta meeting bergabung menggunakan link meeting tersebut dan memasukkan password meeting.\nJika durasi meeting sudah berakhir, meeting akan diakhiri sesuai dengan pembelian paket.",
      isPublished: true,
      isFeatured: true
    },
    {
      id: 4,
      title: "#DiRumahTerusTerhibur dengan MAXstream!",
      subtitle: "#NonstopNonton di rumah sampai puas",
      description: "MAXstream adalah aplikasi video yang menampilkan ribuan film dan serial TV dari MAXstream Original, HBO GO, MyPlay, NOMO, Starvision, Sushiroll, Vidio, VIU, WeTV iflix, dan beragam tayangan menarik lainnya.\nMAXstream juga menyiarkan langsung pertandingan olahraga dari Top Liga Eropa, seperti Serie A (Liga Italia), La Liga (Liga Spanyol), Ligue 1 France (Liga Prancis), FA Cup (Piala FA), Pertandingan MMA dari ONE Championship, dan lain lain.\nSelain ribuan film dan serial TV, MAXstream menyediakan berbagai channel, seperti NET TV, RTV, RCTI, beIN SPORTS, WARNER TV, HBO, dan banyak lagi.\nNikmati berbagai tayangan favorit di MAXstream. Aktifkan paket MAXstream mulai dari Rp6.500 dan dapatkan kuota hingga 40GB termasuk akses premium. Nikmati akses MAXstream, Disney+ Hotstar, Netflix, hingga TikTok dengan Kuota MAXstream!.",
      tnc: "Setiap pembelian paket MAXstream maka Kuota MAXstream akan berlaku sesuai dengan periode pembelian paket (tidak berlaku akumulasi).\nJika Kuota MAXstream sudah habis sebelum masa berlaku, pelanggan masih tetap bisa menikmati tayangan film dan serial di aplikasi MAXstream menggunakan kuota Internet, OMG!, atau Entertainment.\nPelanggan dapat membeli paket MAXstream tanpa batasan jumlah pembelian.",
      isPublished: true,
      isFeatured: true
    }
  ]
    await queryInterface.bulkInsert("News", news);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('News', null, {});
  }
};
