"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const workflows = [
      {
        id: 1,
        name: "WAITING-PAYMENT",
        description: "Menunggu Customer melakukan pembayaran",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 2,
        name: "PAID",
        description:
          "Customer sudah melakukan pembayaran menggunakan midtrans, loan, wallet, atau manual transfer yang sudah diverifikasi admin",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 3,
        name: "MERCHANT-PROGRESS",
        description: "Merchant mempersiapkan paket untuk dikirim",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 4,
        name: "SHIPPED",
        description: "Barang sudah diserahkan ke expedisi",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 5,
        name: "FINISH",
        description: "Barang sudah diterima oleh Customer",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 6,
        name: "CANCEL",
        description: "Order dibatalkan oleh Customer",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: 7,
        name: "MERCHANT-DECLINED",
        description: "Merchant membatalkan pesanan dari Customer",
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
    ];
    await queryInterface.bulkInsert("Workflows", workflows);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
