"use strict";
const RajaOngkirRepository = require("../../src/repository/rajaongkir.repository");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const rajaOngkirRepository = new RajaOngkirRepository();
    const response = await rajaOngkirRepository.getListProvinceFromRajaOngkir();
    const provinces = response.results.map((data) => {
      return {
        id: data.province_id,
        name: data.province,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      };
    });
    await queryInterface.bulkInsert("Provinces", provinces);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
