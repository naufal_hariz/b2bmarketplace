"use strict";
const { v4: uuidv4 } = require("uuid");
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const users = [
      {
        id: uuidv4(),
        name: "Dummy Admin B2B",
        email: "dummy_adm_b2b@gmail.com",
        password: await bcrypt.hash("adminb2b", 10),
        phone: "081122334455",
        address: "Mampang Prapatan 1 No. 5 Jakarta Selatan",
        provinceId: 6,
        cityId: 153,
        loanLimit: 1000000,
        loanBalance: 0,
        cashBalance: 0,
        isActive: true,
        isAdminB2B: true,
        isAdminMerchant: false,
        isCustomer: false,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: uuidv4(),
        name: "Dummy Admin Merchant",
        email: "dummy_adm_merchant@gmail.com",
        password: await bcrypt.hash("adminmerchant", 10),
        address: "Jl. Tuparev No. 11 Cirebon",
        phone: "081122334455",
        provinceId: 9,
        cityId: 109,
        loanLimit: 1000000,
        loanBalance: 0,
        cashBalance: 0,
        isActive: true,
        isAdminB2B: false,
        isAdminMerchant: true,
        isCustomer: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
      {
        id: uuidv4(),
        name: "Dummy Customer",
        email: "dummy_customer@gmail.com",
        password: await bcrypt.hash("customer", 10),
        phone: "081122334455",
        address: "Perum Putri Juanda Sidoarjo",
        provinceId: 11,
        cityId: 409,
        loanLimit: 1000000,
        loanBalance: 0,
        cashBalance: 0,
        isActive: true,
        isAdminB2B: false,
        isAdminMerchant: false,
        isCustomer: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      },
    ];
    await queryInterface.bulkInsert("Users", users);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
