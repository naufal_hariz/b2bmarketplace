'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.addColumn("Topups", "paymentStatus", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Topups", "verificationStatus", {
      type: Sequelize.STRING
    });    
    await queryInterface.addColumn("Topups", "verificationTime", {
      type: Sequelize.DATE
    });    
    await queryInterface.addColumn("Topups", "verificationBy", {
      type: Sequelize.UUID
    });    
    await queryInterface.addColumn("Topups", "verificationNotes", {
      type: Sequelize.STRING
    });        

    await queryInterface.addColumn("LoanPayments", "paymentStatus", {
      type: Sequelize.STRING
    });    
    await queryInterface.addColumn("LoanPayments", "verificationStatus", {
      type: Sequelize.STRING
    });    
    await queryInterface.addColumn("LoanPayments", "verificationTime", {
      type: Sequelize.DATE
    });    
    await queryInterface.addColumn("LoanPayments", "verificationBy", {
      type: Sequelize.UUID
    });    
    await queryInterface.addColumn("LoanPayments", "verificationNotes", {
      type: Sequelize.STRING
    });        

    await queryInterface.addColumn("Orders", "paymentStatus", {
      type: Sequelize.STRING
    });    
    await queryInterface.addColumn("Orders", "verificationStatus", {
      type: Sequelize.STRING
    });    
    await queryInterface.addColumn("Orders", "verificationTime", {
      type: Sequelize.DATE
    });    
    await queryInterface.addColumn("Orders", "verificationBy", {
      type: Sequelize.UUID
    });    
    await queryInterface.addColumn("Orders", "verificationNotes", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Orders", "shippingETD", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Orders", "shippingWeight", {
      type: Sequelize.FLOAT
    });



  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
