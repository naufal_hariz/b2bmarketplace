'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Orders', {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID
      },
      merchantId: {
        type: Sequelize.UUID,
        references: {
          model: 'Merchants',
          key: 'id'
        }
      },
      customerId: {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      orderTime: {
        type: Sequelize.DATE
      },
      shippingAddress: {
        type: Sequelize.STRING
      },
      shippingPartner: {
        type: Sequelize.STRING
      },
      shippingService: {
        type: Sequelize.STRING
      },
      shippingCost: {
        type: Sequelize.INTEGER
      },
      shippingTrackingNumber: {
        type: Sequelize.STRING
      },
      invoiceNumber: {
        type: Sequelize.STRING
      },
      currentOrderStatus: {
        type: Sequelize.STRING
      },
      paymentTime: {
        type: Sequelize.STRING
      },
      paymentMethod: {
        type: Sequelize.STRING
      },
      paymentTo: {
        type: Sequelize.STRING
      },
      paymentNominal: {
        type: Sequelize.INTEGER
      },
      paymentProof: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        onUpdate : Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      deletedAt: {
          type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Orders');
  }
};