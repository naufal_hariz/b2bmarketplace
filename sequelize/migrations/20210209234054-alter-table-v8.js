'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Versions", "isReleased", {
      type: Sequelize.BOOLEAN
    });

    await queryInterface.addColumn("News", "thumbnail", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("News", "publishedDate", {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn("News", "tagId", {
      type: Sequelize.INTEGER,
      references: {
        model: 'Categories',
        key: 'id'
      }
    });

    await queryInterface.removeColumn("Users", "houseNumber");

    await queryInterface.addColumn("Merchants", "status", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Merchants", "verificationNotes", {
      type: Sequelize.STRING
    });

    await queryInterface.removeColumn("Loans", "merchantId");
    await queryInterface.addColumn("Loans", "verificationNotes", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Loans", "status", {
      type: Sequelize.STRING
    });

    await queryInterface.removeColumn("Orders", "orderTime");
    await queryInterface.removeColumn("Orders", "currentOrderStatus");
    await queryInterface.addColumn("Orders", "submittedTime", {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn("Orders", "invoiceNumber", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Orders", "workflowId", {
      type: Sequelize.INTEGER,
      references: {
        model: 'Workflows',
        key: 'id'
      }
    });

    await queryInterface.addColumn("Products", "feature", {
      type: Sequelize.TEXT
    });
    await queryInterface.addColumn("Products", "banner", {
      type: Sequelize.STRING
    });
    await queryInterface.addColumn("Products", "isHot", {
      type: Sequelize.BOOLEAN
    });
    await queryInterface.addColumn("Products", "isFavourite", {
      type: Sequelize.BOOLEAN
    });
    await queryInterface.addColumn("Products", "isTop", {
      type: Sequelize.BOOLEAN
    });

    await queryInterface.addColumn("OrderWorkFlows", "workflowId", {
      type: Sequelize.INTEGER,
      references: {
        model: 'Workflows',
        key: 'id'
      }
    });

    await queryInterface.addColumn("OrderDetails", "weight", {
      type: Sequelize.FLOAT
    });

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
