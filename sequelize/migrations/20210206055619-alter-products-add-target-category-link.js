"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Products", "target", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Products", "category", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Products", "link", {
      type: Sequelize.STRING,
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("Products", "target");
    await queryInterface.removeColumn("Products", "category");
    await queryInterface.removeColumn("Products", "link");
  },
};
