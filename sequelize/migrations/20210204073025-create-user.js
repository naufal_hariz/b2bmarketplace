"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Users", {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID,
      },
      name: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      address: {
        type: Sequelize.STRING,
      },
      houseNumber: {
        type: Sequelize.INTEGER,
      },
      provinceId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Provinces",
          key: "id",
        },
      },
      cityId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Cities",
          key: "id",
        },
      },
      photo: {
        type: Sequelize.STRING,
      },
      identityNumber: {
        type: Sequelize.STRING,
      },
      identityPhoto: {
        type: Sequelize.STRING,
      },
      identityPhotoSelfie: {
        type: Sequelize.STRING,
      },
      cashBalance: {
        type: Sequelize.INTEGER,
      },
      loanLimit: {
        type: Sequelize.INTEGER,
      },
      loanBalance: {
        type: Sequelize.INTEGER,
      },
      isActive: {
        type: Sequelize.BOOLEAN,
      },
      isAdminB2B: {
        type: Sequelize.BOOLEAN,
      },
      isAdminMerchant: {
        type: Sequelize.BOOLEAN,
      },
      isCustomer: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
        onUpdate: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      deletedAt: {
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Users");
  },
};
