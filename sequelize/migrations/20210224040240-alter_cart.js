'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn("Carts", "isChecked", {
            type: Sequelize.BOOLEAN,
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn("Carts", "isChecked");
    }
};