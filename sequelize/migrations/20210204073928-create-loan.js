'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Loans', {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID
      },
      merchantId: {
        type: Sequelize.UUID,
        references: {
          model: 'Merchants',
          key: 'id'
        }
      },
      customerId: {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      submittedTime: {
        type: Sequelize.DATE
      },
      nominal: {
        type: Sequelize.INTEGER
      },
      tenor: {
        type: Sequelize.STRING
      },
      dueDate: {
        type: Sequelize.DATE
      },
      installment: {
        type: Sequelize.STRING
      },
      responseStatus: {
        type: Sequelize.STRING
      },
      responseTime: {
        type: Sequelize.DATE
      },
      responseBy: {
        type: Sequelize.STRING
      },
      totalPaid: {
        type: Sequelize.INTEGER
      },
      currentBalance: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
          type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Loans');
  }
};