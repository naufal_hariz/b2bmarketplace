'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Cart extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Product, {
                foreignKey: "productId",
                as: "product",
            });
            this.belongsTo(models.Merchant, {
                foreignKey: "merchantId",
                as: "merchant",
            });
        }
    };
    Cart.init({
        userId: DataTypes.STRING,
        merchantId: DataTypes.UUID,
        productId: DataTypes.INTEGER,
        qty: DataTypes.INTEGER,
        isChecked: DataTypes.BOOLEAN
    }, {
        sequelize,
        modelName: 'Cart',
    });
    return Cart;
};