"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class City extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Merchant, {
        foreignKey: "cityId",
      });
      this.hasMany(models.User, {
        foreignKey: "cityId",
      });
      this.belongsTo(models.Province, {
        foreignKey: "provinceId",
        as: "province",
      });
    }
  }
  City.init(
    {
      provinceId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      type: DataTypes.STRING,
      postalCode: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "City",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return City;
};
