"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class OrderWorkflow extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Workflow, {
        foreignKey: "workflowId",
        as: "workflow",
      });
    }
  }
  OrderWorkflow.init(
    {
      orderId: DataTypes.UUID,
      workflowTime: DataTypes.DATE,
      workflowId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "OrderWorkflow",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return OrderWorkflow;
};
