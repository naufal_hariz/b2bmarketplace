'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Logger extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Logger.init({
    method: DataTypes.STRING,
    url: DataTypes.STRING,
    userAgent: DataTypes.STRING,
    ip: DataTypes.STRING,
    reqData: DataTypes.TEXT,
    resData: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Logger',
  });
  return Logger;
};