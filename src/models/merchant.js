"use strict";
const { Model } = require("sequelize");

const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  class Merchant extends Model {
    static associate(models) {
      // this.hasMany(models.Loan, {
      //   foreignKey: "customerId",
      //   as: "reviews",
      // });
      // this.hasMany(models.Merchant, {
      //   foreignKey: "adminMerchantId",
      //   as: "merchants",
      // });
      // this.hasMany(models.Topup, {
      //   foreignKey: "customerId",
      //   as: "topups",
      // });
      this.belongsToMany(models.Product, {
        through: "MerchantProducts",
        foreignKey: "merchantId",
        as: "products",
      });
      this.belongsTo(models.User, {
        foreignKey: "adminMerchantId",
        as: "user",
      });
      this.belongsTo(models.Province, {
        foreignKey: "provinceId",
        as: "province",
      });
      this.belongsTo(models.City, {
        foreignKey: "cityId",
        as: "city",
      });
    }
  }
  Merchant.init(
    {
      name: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.STRING,
      provinceId: DataTypes.INTEGER,
      cityId: DataTypes.INTEGER,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      photo: DataTypes.STRING,
      adminMerchantId: DataTypes.INTEGER,
      verificationStatus: DataTypes.STRING /* PENDING | ACTIVE | INACTIVE */,
      verificationTime: DataTypes.STRING,
      verificationBy: DataTypes.STRING,
      cashBalance: DataTypes.INTEGER,
      verificationNotes: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Merchant",
      paranoid: true,
    }
  );

  Merchant.beforeCreate((table) => (table.id = uuidv4()));

  return Merchant;
};
