"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {

    /*
    * status : { Waiting Approval, Active , Inactive}
    * Inactive === Rejected
    */

    class Loan extends Model {
        static associate(models) {
            this.hasMany(models.LoanPayment, {
                foreignKey: "loanId",
                as: "loanPayments",
            });

            this.belongsTo(models.User, {
                foreignKey: "customerId",
                as: "customer",
            });

            this.belongsTo(models.Order, {
                foreignKey: "orderId",
                as: "order",
            });
        }
    }
    Loan.init(
        {
            customerId: DataTypes.UUID,
            orderId: DataTypes.UUID,
            submittedTime: DataTypes.DATE,
            nominal: DataTypes.FLOAT,
            tenor: DataTypes.INTEGER,
            dueDate: DataTypes.DATE,
            installment: DataTypes.FLOAT,
            responseStatus: DataTypes.STRING,
            responseTime: DataTypes.DATE,
            responseBy: DataTypes.UUID,
            totalPaid: DataTypes.FLOAT,
            currentBalance: DataTypes.FLOAT,
            status: DataTypes.STRING,
            verificationNotes: DataTypes.TEXT,
        },
        {
            sequelize,
            modelName: "Loan",
            paranoid: true,
            defaultScope: {
                attributes: {
                    exclude: ["createdAt", "updatedAt", "deletedAt"],
                },
            },
        }
    );
    return Loan;
};
