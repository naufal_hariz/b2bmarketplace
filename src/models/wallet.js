"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Wallet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Wallet.init(
    {
      customerId: DataTypes.INTEGER,
      transactionTime: DataTypes.DATE,
      description: DataTypes.STRING,
      type: DataTypes.STRING,
      nominal: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "Wallet",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Wallet;
};
