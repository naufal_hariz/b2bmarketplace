"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Topup extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Topup.init(
    {
      customerId: DataTypes.INTEGER,
      paymentTime: DataTypes.DATE,
      paymentMethod: DataTypes.STRING,
      paymentTo: DataTypes.STRING,
      paymentNominal: DataTypes.INTEGER,
      paymentProof: DataTypes.STRING,
      paymentStatus: DataTypes.STRING,
      verificationStatus: DataTypes.STRING,
      verificationTime: DataTypes.DATE,
      verificationBy: DataTypes.UUID,
      verificationNotes: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Topup",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Topup;
};
