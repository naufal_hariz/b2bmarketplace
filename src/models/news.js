'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class News extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  News.init({
    title: DataTypes.STRING,
    subtitle: DataTypes.STRING,
    description: DataTypes.TEXT,
    tnc: DataTypes.TEXT,
    isPublished: DataTypes.BOOLEAN,
    isFeatured: DataTypes.BOOLEAN,
    thumbnail: DataTypes.STRING
    // publishedDate:DataTypes.DATE,
    // tagId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'News',
    paranoid: true
  });
  return News;
};