"use strict";
const { Model } = require("sequelize");

const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      this.hasMany(models.Review, {
        foreignKey: "customerId",
        as: "reviews",
      });
      this.hasMany(models.Merchant, {
        foreignKey: "adminMerchantId",
        as: "merchants",
      });
      this.hasMany(models.Topup, {
        foreignKey: "customerId",
        as: "topups",
      });
      this.hasMany(models.Loan, {
        foreignKey: "customerId",
        as: "loans",
      });
      this.hasMany(models.Order, {
        foreignKey: "customerId",
        as: "orders",
      });
      this.belongsTo(models.Province, {
        foreignKey: "provinceId",
        as: "province",
      });
      this.belongsTo(models.City, {
        foreignKey: "cityId",
        as: "city",
      });
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.STRING,
      provinceId: DataTypes.INTEGER,
      cityId: DataTypes.INTEGER,
      photo: DataTypes.STRING,
      identityNumber: DataTypes.STRING,
      identityPhoto: DataTypes.STRING,
      identityPhotoSelfie: DataTypes.STRING,
      cashBalance: DataTypes.INTEGER,
      loanLimit: DataTypes.INTEGER,
      loanBalance: DataTypes.INTEGER,
      isActive: DataTypes.BOOLEAN,
      isAdminB2B: DataTypes.BOOLEAN,
      isAdminMerchant: DataTypes.BOOLEAN,
      isAdminMerchant: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "User",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );

  User.beforeCreate((table) => (table.id = uuidv4()));

  return User;
};
