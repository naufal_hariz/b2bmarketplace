"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class MerchantProduct extends Model {
    static associate(models) {
      // this.hasMany(models.Merchant, {
      //   foreignKey: "merchantId",
      //   as: "merchants",
      // });
      // this.hasMany(models.Product, {
      //   foreignKey: "productId",
      //   as: "products",
      // });
      this.belongsTo(models.Merchant, {
        foreignKey: "merchantId",
        as: "merchant",
      });

      this.belongsTo(models.Product, {
        foreignKey: "productId",
        as: "product",
      });
    }
  }
  MerchantProduct.init(
    {
      merchantId: DataTypes.UUID,
      productId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "MerchantProduct",
      paranoid: true,
    }
  );
  return MerchantProduct;
};
