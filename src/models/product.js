"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    static associate(models) {
      this.belongsTo(models.Category, {
        foreignKey: "categoryId",
        as: "category",
      });

      this.hasMany(models.Review, {
        foreignKey: "productId",
        as: "reviews",
      });

      this.belongsToMany(models.Merchant, {
        through: "MerchantProducts",
        foreignKey: "productId",
        as: "merchants",
      });

      this.belongsToMany(models.Order, {
        through: "OrderDetails",
        foreignKey: "productId",
        as: "orders",
      });

      this.hasMany(models.ProductGallery, {
        foreignKey: "productId",
        as: "productGalleries",
      });
      this.hasMany(models.OrderDetail, {
        foreignKey: "productId",
        as: "orderdetails",
      });
    }
  }
  Product.init(
    {
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      feature: DataTypes.TEXT,
      banner: DataTypes.TEXT,
      weight: DataTypes.FLOAT,
      price: DataTypes.FLOAT,
      isNew: DataTypes.BOOLEAN,
      isPopular: DataTypes.BOOLEAN,
      isRecommended: DataTypes.BOOLEAN,
      isHot: DataTypes.BOOLEAN,
      isFavourite: DataTypes.BOOLEAN,
      isTop: DataTypes.BOOLEAN,
      countRating: DataTypes.FLOAT,
      averageRating: DataTypes.FLOAT,
      target: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Product",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Product;
};
