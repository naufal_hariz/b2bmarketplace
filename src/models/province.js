"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Province extends Model {
    static associate(models) {
      this.hasMany(models.User, {
        foreignKey: "provinceId",
        as: "users",
      });
      this.hasMany(models.City, {
        foreignKey: "provinceId",
        as: "cities",
      });
      this.hasMany(models.Merchant, {
        foreignKey: "provinceId",
        as: "merchants",
      });
    }
  }
  Province.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Province",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Province;
};
