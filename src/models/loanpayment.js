"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class LoanPayment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Loan, {
        foreignKey: "loanId",
        as: "loan",
      });
    }
  }
  LoanPayment.init(
    {
      loanId: DataTypes.INTEGER,
      paymentTime: DataTypes.DATE,
      paymentMethod: DataTypes.STRING,
      paymentTo: DataTypes.STRING,
      paymentNominal: DataTypes.FLOAT,
      paymentProof: DataTypes.STRING,
      paymentStatus: DataTypes.STRING,
      verificationStatus: DataTypes.STRING,
      verificationTime: DataTypes.DATE,
      verificationBy: DataTypes.UUID,
      verificationNotes: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "LoanPayment",
      paranoid: true,
    }
  );
  return LoanPayment;
};
