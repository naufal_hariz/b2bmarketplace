"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    static associate(models) {
      this.hasMany(models.OrderDetail, {
        foreignKey: "orderId",
        as: "orderDetails",
      });

      this.hasMany(models.OrderWorkflow, {
        foreignKey: "orderId",
        as: "orderWorkflows",
      });

      this.belongsTo(models.Workflow, {
        foreignKey: "workflowId",
        as: "workflow",
      });

      //sementara dibuat hasMany dulu, supaya menggunakan orderId yg sama pun tak masalah. Soalnya orderIdnya masih masuk sprint2
      this.hasMany(models.Loan, {
        foreignKey: "orderId",
        as: "loans",
      });

      this.belongsTo(models.User, {
        foreignKey: "customerId",
        as: "customer",
      });

      this.belongsTo(models.Merchant, {
        foreignKey: "merchantId",
        as: "merchant",
      });

      this.belongsToMany(models.Product, {
        through: "OrderDetails",
        foreignKey: "orderId",
        as: "products",
      });
    }
  }
  Order.init(
    {
      merchantId: DataTypes.UUID,
      customerId: DataTypes.UUID,
      shippingAddress: DataTypes.STRING,
      shippingPartner: DataTypes.STRING,
      shippingService: DataTypes.STRING,
      shippingWeight: DataTypes.FLOAT,
      shippingCost: DataTypes.FLOAT,
      shippingTrackingNumber: DataTypes.STRING,
      invoiceNumber: DataTypes.STRING,
      paymentTime: DataTypes.DATE,
      paymentMethod: DataTypes.STRING, // {manual_transfer / midtrans}
      paymentTo: DataTypes.STRING,
      paymentNominal: DataTypes.FLOAT,
      paymentProof: DataTypes.STRING,
      submittedTime: DataTypes.DATE,
      invoiceNumber: DataTypes.STRING,
      workflowId: DataTypes.INTEGER,
      paymentStatus: DataTypes.STRING,
      verificationStatus: DataTypes.STRING, 
      verificationTime: DataTypes.DATE,
      verificationBy: DataTypes.UUID,
      verificationNotes: DataTypes.STRING,
      shippingETD: DataTypes.STRING,
      totalItem: DataTypes.INTEGER,
      totalAmount: DataTypes.FLOAT,      
    },
    {
      sequelize,
      modelName: "Order",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Order;
};
