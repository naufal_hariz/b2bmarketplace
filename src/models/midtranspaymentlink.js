'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MidtransPaymentLink extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  MidtransPaymentLink.init({
    invoice: DataTypes.STRING,
    link: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MidtransPaymentLink',
  });
  return MidtransPaymentLink;
};