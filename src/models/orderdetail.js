"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class OrderDetail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Product, {
        foreignKey: "productId",
        as: "product",
      });

      this.belongsTo(models.Order, {
        foreignKey: "orderId",
        as: "order",
      });
    }
  }
  OrderDetail.init(
    {
      productId: DataTypes.INTEGER,
      orderId: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      price: DataTypes.FLOAT,
      tax: DataTypes.FLOAT,
      discount: DataTypes.FLOAT,
      total: DataTypes.FLOAT,
      weight: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "OrderDetail",
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return OrderDetail;
};
