'use strict';
const {
    Model
} = require('sequelize');
const { v4: uuidv4 } = require('uuid');

module.exports = (sequelize, DataTypes) => {
    class __test_table extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    __test_table.init({
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        title: DataTypes.STRING
    }, {
        sequelize,
        modelName: '__test_table',
        paranoid: true
    });

    __test_table.beforeCreate(table => table.id = uuidv4());

    return __test_table;
};