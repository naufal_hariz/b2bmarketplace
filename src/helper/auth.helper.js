const db = require('../models');
const { user } = require("../models");

class userDatabase {
    async getUser(email) {
        try {
            const resultEmail = await db.User.findOne({
                where: { email },
            });
            if (resultEmail != null) {
                return { success: true, result: resultEmail, status: 200 };
            }
            else {
                return { success: false, result: resultEmail, status: 500 };
            }
        } catch (error) {
            return { success: false, result: error, status: 500 };
        }
    }

    async getUserById(id) {
        try {
            const resultUsername = await db.User.findOne({
                where: { id: id },
                include : ['city','province']
            });
            if (resultUsername != null) {
                return { success: true, result: resultUsername, status: 200 };
            }
            else {
                return { success: false, result: resultUsername, status: 500 };
            }
        } catch (error) {
            return { success: false, result: error, status: 500 };
        }
    }
    async updateUser(){
        try {
            const resultUser = await db.user.update(dataUser)
            return { success: true, result: resultUser, status: 200 };

        } catch (error) {
            return { success: false, result: error, status: 500 };
        }
    }
}

module.exports = new userDatabase()