const db = require('../models');

class logDatabase {
    async storeLog(data) {
        try {
            const result = await db.Logger.create({
                method: data.method,
                url: data.url,
                userAgent: data.userAgent,
                ip: data.ip,
                reqData: data.reqData,
                resData: data.resData,
            })
            return {success: true, result: result}
        } catch (error) {
            return {success: false, result: error}
        }
    }
}

module.exports = new logDatabase()