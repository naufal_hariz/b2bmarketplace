const {
    Order, User,
    Merchant, Product,
} = require("../models");
const { ApiMailjet } = require("../api/api")
const emailFrom = "tobiko@surelin.de"
const emailFromName = "Tobiko"

class invoiceOrder {

    constructor() {
        this.invoice = {};
    }

    formatNumber(e) {
        return e.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }

    async get(id) {
        try {
            const order = await Order.findByPk(id, {
                include: [
                    {
                        model: Product,
                        as: "products",
                        attributes: ["id", "name", "banner"],
                        through: {
                            attributes: ["quantity", "tax", "discount", "total"],
                        },
                    },
                    {
                        model: User,
                        as: "customer",
                        attributes: [
                            "id",
                            "name",
                            "email",
                            "phone",
                            "address",
                            "provinceId",
                            "cityId",
                        ],
                        include: ["province", "city"],
                    },
                    {
                        model: Merchant,
                        as: "merchant",
                        attributes: [
                            "id",
                            "name",
                            "phone",
                            "address",
                            "provinceId",
                            "cityId",
                        ],
                        include: ["province", "city"],
                    },
                    "workflow",
                ],
            });

            const orderDetails = await order.getOrderDetails();
            const totalTransaction = await orderDetails.reduce((total, orderDetail) => {
                return total + orderDetail.total;
            }, 0);

            this.invoice = { order, totalTransaction }

            return this;

        } catch (error) {
            console.log(error)
            return { success: false, result: error }
        }
    }

    async sendEmail() {
        try {
            const data = this.invoice
            let products = []

            data.order.products.forEach(e => {
                let { total, quantity } = e.OrderDetails.dataValues
                products.push({
                    title: e.name,
                    image_URL: e.banner,
                    price: {
                        currency: "Rp. ",
                        separator: ".",
                        amount: this.formatNumber(total)
                    },
                    qty: quantity
                })
            });

            let param = {
                "invoice": data.order.invoiceNumber,
                "customer": data.order.customer.name,
                "email": data.order.customer.email,
                "date": new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                "total": "Rp. " + this.formatNumber(data.totalTransaction),
                "address": data.order.shippingAddress,
                "order": {
                    "items": products,
                }
            }

            const request = ApiMailjet
                .post("send", { 'version': 'v3.1' })
                .request({
                    Messages: [
                        {
                            From: {
                                Email: "tobiko@surelin.de",
                                Name: "Tobiko"
                            },
                            To: [
                                {
                                    Email: data.order.customer.email,
                                    Name: data.order.customer.name
                                }
                            ],
                            Subject: "Tobiko Order : " + data.order.invoiceNumber,
                            TemplateID: 2465421,
                            TemplateLanguage: true,
                            Variables: param

                        }
                    ]
                })
            request.then((result) => {
                console.log(result.body)
            }).catch((err) => {
                console.log(err.statusCode)
            })
            return { success: true, result: param }
        } catch (error) {
            console.log(error)
            return { success: false, result: error }
        }
    }

}

module.exports = new invoiceOrder()