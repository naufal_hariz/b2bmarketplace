const fs = require("fs");
const { v4: uuidv4 } = require("uuid");
const base64Img = require("base64-img");
var Jimp = require("jimp");
const { AWS } = require("../api/api");
const path = require("path");
var mime = require("mime-types");

class uploadHelper {
  async base64(img, thumb = true) {
    try {
      let checkimgJPG = img.includes("data:image/jpeg;base64");
      let checkimgPNG = img.includes("data:image/png;base64");

      if (!(checkimgPNG || checkimgJPG))
        throw "Base 64 not valid, only jpg / png format accepted";

      const imgStorage = `${baseDir}/public/images`;
      const imgStorageThumb = `${baseDir}/public/images/thumb`;
      const extension = checkimgJPG ? ".jpg" : ".png";

      if (!fs.existsSync(imgStorage)) fs.mkdirSync(imgStorage);
      if (!fs.existsSync(imgStorageThumb)) fs.mkdirSync(imgStorageThumb);

      const uuid = uuidv4();

      const slug = "bs64" + uuid;
      let createdFilePath = imgStorage + "/" + slug;
      let ThumbFilePath = imgStorageThumb + "/" + slug;

      base64Img.imgSync(img, imgStorage, slug);
      createdFilePath += extension;

      let file = {};

      if (thumb) {
        await Jimp.read(createdFilePath)
          .then((image) => {
            return image.resize(200, 200).write(ThumbFilePath + extension);
          })
          .catch((err) => {
            fs.unlinkSync(createdFilePath);
            throw "Image Not Valid";
          });
        ThumbFilePath += extension;
        file.thumb = ThumbFilePath;
      }

      file.image = createdFilePath;

      return { success: true, file: file };
    } catch (error) {
      console.log(error);
      return { success: false, result: error };
    }
  }

  async awsStorage(params) {
    try {
      let location = `tobiko/${params.folder}/`;
      if (params.thumb) location += "thumbs/";
      let filepath = params.file;

      var s3 = new AWS.S3();
      var params = {
        Bucket: "dzahin-storage",
        Body: fs.createReadStream(filepath),
        Key: location + path.basename(filepath),
        ACL: "public-read",
        ContentType: mime.lookup(filepath),
      };

      let url = new Promise((resolve, reject) => {
        s3.upload(params, function (err, data) {
          fs.unlinkSync(filepath);
          if (err) {
            return reject(err);
          }
          return resolve(data.Location);
        });
      });
      return await url;
    } catch (error) {
      console.log(error);
      return { success: false, error: error };
    }
  }
}

module.exports = new uploadHelper();
