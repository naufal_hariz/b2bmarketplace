const { MIDTRANSSERVERKEY } = require("../api/api")
const midtransClient = require('midtrans-client')
const { MidtransPaymentLink, Order, User, Loan } = require("../models");
const axios = require('axios')

class paymentMidtrans {

    async pay(orderId, total, mode = 'trx') {
        try {
            let snap = new midtransClient.Snap({
                isProduction: false,
                serverKey: 'SB-Mid-server-G8CXL7nwSY1Ia73rGURxWmyI',
                clientKey: 'SB-Mid-client-uDa0PE0oOQyKHXJs'
            });

            let orderData, invoice, paymentLink, customer

            if (mode === 'trx') {
                orderData = await Order.findByPk(orderId)
                invoice = orderData.invoiceNumber
                paymentLink = await MidtransPaymentLink.findOne({ where: { invoice } })
                customer = await User.findByPk(orderData.customerId)
            } else {
                let loanData = await Loan.findOne({
                    where: { id: orderId },
                    include: ['customer']
                })
                invoice = "lx-" + loanData.id.substring(24, 36) + "-" + loanData.customerId.substring(24, 36)
                customer = await User.findByPk(loanData.customerId)
                paymentLink = false

                MidtransPaymentLink.create({ invoice, link: loanData.id })
            }

            if (paymentLink)
                return { success: true, result: paymentLink.link }

            let parameter = {
                transaction_details: {
                    order_id: invoice,
                    gross_amount: total
                },
                credit_card: {
                    secure: true
                },
                enabled_payments:
                    [
                        "credit_card", "gopay", "akulaku"
                    ],
                customer_details: {
                    first_name: customer.name,
                    email: customer.email,
                    phone: customer.phone
                }
            };

            const options = {
                method: 'post',
                url: 'https://tobiko.dzahin.com/midtrans',
                data: {
                    body: parameter
                }
            };
            let ax = await axios(options).then(res => {
                if (res.status == 200) {
                    if(mode === 'trx')
                        MidtransPaymentLink.create({ invoice, link: res.data.result })
                    return res.data
                } else {
                    return { success: false, result: error }
                }
            })

            return ax

            /*
            ======== server binar blocked midtrans ========

            let link = await snap.createTransaction(parameter)
                .then((transaction) => {
                    let transactionToken = transaction.token;
                    let transactionRedirectUrl = transaction.redirect_url;
                    MidtransPaymentLink.create({ invoice, link: transactionRedirectUrl })
                    return transactionRedirectUrl
                })
                .catch((e) => {
                    console.log(e)
                    if (e.message.indexOf("transaction_details.order_id sudah digunakan"))
                        throw "OrderId already paid on midtrans"

                    throw e.message
                });

            return { success: true, result: link }
            */

        } catch (error) {
            return { success: false, result: error }
        }
    }
}

module.exports = new paymentMidtrans()