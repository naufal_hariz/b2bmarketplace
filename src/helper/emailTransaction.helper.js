const { ApiMailjet } = require("../api/api")

const db = require('../models');
const emailFrom = "tobiko@surelin.de"
const emailFromName = "Tobiko"

class emailTransation {

    /*
    *params*
    *   data = {
    *           to : {
    *                   name : name of recipient,
    *                   email : email of recipient,
    *               },
    *           subject : subject of email,
    *           link : activation link
    *   }
    */

    async sendActivateUser(params) {
        try {
            const request = ApiMailjet
                .post("send", { 'version': 'v3.1' })
                .request({
                    Messages: [
                        {
                            From: {
                                Email: "tobiko@surelin.de",
                                Name: "Tobiko"
                            },
                            To: [
                                {
                                    Email: params.to.email,
                                    Name: params.to.name
                                }
                            ],
                            Subject: params.subject,
                            TemplateID: params.template,
                            TemplateLanguage: true,
                            Variables: {
                                "name": params.to.name,
                                "link": params.link
                            }
                        }
                    ]
                })
            request
                .then((result) => {
                    console.log(result.body)
                })
                .catch((err) => {
                    console.log(err.statusCode)
                })
            return { success: true, result: result.body }
        } catch (error) {
            return { success: false, result: error }
        }
    }

    async sendOrderNotif(order) {
        try {
            const request = ApiMailjet
                .post("send", { 'version': 'v3.1' })
                .request({
                    Messages: [
                        {
                            From: {
                                Email: "tobiko@surelin.de",
                                Name: "Tobiko"
                            },
                            To: [
                                {
                                    Email: order.customer.email,
                                    Name: order.customer.name
                                }
                            ],
                            Subject: "Tobiko Order Notification : " + order.workflow.name + " - " + order.invoiceNumber,
                            TemplateID: 2461909,
                            TemplateLanguage: true,
                            Variables: {
                                "name": order.customer.name,
                                "orderstatus": order.workflow.name,
                                "invoice": order.invoiceNumber
                            }
                        }
                    ]
                })
            request
                .then((result) => {
                    console.log(result.body)
                })
                .catch((err) => {
                    console.log(err.statusCode)
                })
            return { success: true, result: result.body }
        } catch (error) {
            return { success: false, result: error }
        }
    }
}

module.exports = new emailTransation()