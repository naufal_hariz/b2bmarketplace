const RajaOngkirRepository = require("../repository/rajaongkir.repository");

module.exports = {
  async listServiceDetail(data) {
    const rajaOngkirRepository = new RajaOngkirRepository();
    const dataAsReqRajaOngkir = {
      origin: data.originCityId,
      destination: data.destinationCityId,
      weight: data.weight,
      courier: data.shippingPartner,
    };
    const result = await rajaOngkirRepository.postBrowseServices(
      dataAsReqRajaOngkir
    );
    return result;
  },
};
