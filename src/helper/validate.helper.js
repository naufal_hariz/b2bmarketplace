class validateInput {
    email(email) {
        console.log(email)
        const rgxEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return rgxEmail.test(email);
    }

    notNull(input){
        return (input !== "" && input !== undefined) ? true : false
    }

    numberInteger(input){
        return Number.isInteger(input)
    }
}

module.exports = new validateInput()