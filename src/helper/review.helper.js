const { review, movie, Sequelize } = require("../../models");

class reviewHelper {
  async calculateRating(movieID) {
    try {
      let rt = await review.findAll({
        where: { movie_id: movieID },
        attributes: [
          [Sequelize.fn("AVG", Sequelize.col("rating")), "rating_average"],
          [Sequelize.fn("count", Sequelize.col("id")), "rating_count"],
        ],
        raw: true,
        group: ["movie_id"],
      });
      rt[0].rating_average = Math.round(rt[0].rating_average * 2) / 2;
      movie.update(rt[0], { where: { id: movieID } });
      return rt[0];
    } catch (error) {
      console.log(error);
    }
  }

  async calculateAll() {
    let rt = await review.findAll({
      attributes: [
        ["movie_id", "movie_id"],
        [Sequelize.fn("AVG", Sequelize.col("rating")), "rating_average"],
        [Sequelize.fn("count", Sequelize.col("id")), "rating_count"],
      ],
      raw: true,
      group: ["movie_id"],
    });

    for (const i in rt) {
      rt[i].rating_average = Math.round(rt[i].rating_average * 2) / 2;
      movie.update(rt[i], { where: { id: rt[i].movie_id } });
    }
  }
}

module.exports = new reviewHelper();
