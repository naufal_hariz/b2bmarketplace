const express = require("express")
const router = express.Router()

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("user"));

const WalletController = require("../../logic/wallet/wallet.controller");
const walletController = new WalletController();

router.get("/", apikeyMiddleware, walletController.balance);
router.get("/history", apikeyMiddleware, walletController.history);

module.exports = router;