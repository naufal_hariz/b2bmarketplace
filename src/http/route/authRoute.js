const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");

const userController = require("../../logic/user/user.controller");
const authController = require("../../logic/auth/auth.controller");

const uc = new userController();
const ac = new authController();

router.post("/forgot", apikeyMiddleware, uc.forgot);
router.post("/", apikeyMiddleware, ac.auth);

module.exports = router;