const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("user"));

const TopupController = require("../../logic/topup/topup.controller");
const topupController = new TopupController();

router.post("/", topupController.submitManualTopup);

module.exports = router;
