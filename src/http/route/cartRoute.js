const express = require("express")
const router = express.Router()

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
const { apikeyMiddleware } = require("../middleware/APIKEY.middleware")

router.use(apikeyMiddleware)
router.use(AUTHmiddleware("user"));

const cartController = require("../../logic/cart/cart.controller");
const cc = new cartController();

router.get("/", cc.show);
router.post("/", cc.save);
router.delete("/", cc.delete);
router.delete("/all", cc.deleteAll);
router.post("/checks", cc.check);

module.exports = router;