const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);


const ProductController = require("../../logic/product/product.controller");
const productController = new ProductController();

router.get("/", productController.listProduct);
router.get("/latest", productController.latestProduct);
router.get("/category/:categoryId", productController.listProductByCategory);
router.get("/featured/:feature", productController.listProductByFeatured);
router.get("/detail/:id", productController.detailProduct);
router.post("/single/:id", productController.uploadSinglePhoto); //buat testing aja
router.post("/bulk/:id", productController.uploadBulkPhoto); //buat testing aja

const ProductGalleryController = require("../../logic/product/productGallery.controller");
const productGalleryController = new ProductGalleryController();

router.post("/upload", productGalleryController.uploadImageProduct);

module.exports = router;
