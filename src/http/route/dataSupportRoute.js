const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const DistrictController = require("../../logic/data_support/district.controller");
const districtController = new DistrictController();

const ShippingController = require("../../logic/data_support/shipping.controller");
const shippingController = new ShippingController();

const MerchantController = require("../../logic/merchant/merchant.controller");
const merchantController = new MerchantController();

router.get("/district", districtController.listDistrict);
router.get("/province", districtController.listProvince);
router.get("/city/:id", districtController.listCity);
router.get("/city/detail/:id", districtController.detailCity);
router.post("/shipping", shippingController.listServices);
router.get("/merchant", merchantController.list);
router.get("/merchant/:id", merchantController.detail);

module.exports = router;
