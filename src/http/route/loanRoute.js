const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("user"));

const LoanController = require("../../logic/loan/loan.controller");
const loanController = new LoanController();

router.post("/apply", loanController.submitLoan);
router.get("/summary", loanController.summaryLoan);
router.get("/payment", loanController.nextPaymentLoan);
router.get("/history/:loanId", loanController.historyPayment);
router.put("/approve/:id", loanController.approveLoan); //buat testing sendiri aja
router.put("/reject/:id", loanController.rejectLoan); //buat testing sendiri aja
router.post("/payment/:loanId", loanController.submitManualPayment); //buat testing aja

module.exports = router;
