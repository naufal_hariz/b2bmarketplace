const express = require("express");
const router = express.Router();

const LoanController = require("../../logic/loan/loan.controller")
const loanController = new LoanController()

const PaymentController = require("../../logic/payment/payment.controller")
const paymentController = new PaymentController();

const MerchantController = require("../../logic/merchant/merchant.controller")
const merchantController = new MerchantController()

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("admin"));

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware")
router.use(apikeyMiddleware);

router.get("/merchant", merchantController.list);
router.get("/merchant/detail/:id", merchantController.detail);

router.put("/merchant/enable/:id", merchantController.enable);
router.put("/merchant/disable/:id", merchantController.disable);
router.delete("/merchant/:id", merchantController.delete);

router.get("/loan", loanController.list);
router.post("/loan/approve/:id", loanController.approveLoan);
router.post("/loan/reject/:id", loanController.rejectLoan);

router.get("/payment", paymentController.trxManualList);
router.post("/payment/approve/:id", paymentController.trxManualApproved);
router.post("/payment/reject/:id", paymentController.trxManualRejected);

router.get("/payment/loan", paymentController.trxLoanManualList);
router.post("/payment/loan/approve/:id", paymentController.trxLoanManualApproved);
router.post("/payment/loan/reject/:id", paymentController.trxLoanManualReject);

module.exports = router;