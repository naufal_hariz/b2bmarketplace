const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("user"));

const PaymentController = require("../../logic/payment/payment.controller");
const paymentController = new PaymentController();

router.post("/trx/midtrans", apikeyMiddleware, paymentController.trxMidtrans);
router.post("/trx/transfer", apikeyMiddleware, paymentController.trxTrasnfer);
router.post("/trx/wallet", apikeyMiddleware, paymentController.trxWallet);

router.post("/loan/midtrans", apikeyMiddleware, paymentController.loanMidtrans);
router.post("/loan/transfer", apikeyMiddleware, paymentController.loanTrasnfer);
router.post("/loan/wallet", apikeyMiddleware, paymentController.loanWallet);

module.exports = router;