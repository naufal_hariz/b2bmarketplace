const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")
router.use(AUTHmiddleware("user"));

const { orderMiddleware } = require("../middleware/order.middleware");

const OrderController = require("../../logic/order/order.controller");
const orderController = new OrderController();

router.get("/invoice/:id", orderController.getInvoice);
router.get("/timeline/:id", orderController.getTimeline);
router.post("/payment/loan/:id", orderController.paymentUsingLoanBalance);

router.post("/", apikeyMiddleware, orderMiddleware, orderController.submitOrder);
router.get("/", apikeyMiddleware, orderController.getOrderInfo);
router.get("/detail/:id", apikeyMiddleware, orderController.customerOrderDetail);
router.put("/finish/:id", apikeyMiddleware, orderController.finishOrder)


module.exports = router;
