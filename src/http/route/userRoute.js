const express = require("express")
const router = express.Router()

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")

const { validateFrmLoginMiddleware } = require("../middleware/register.middleware")

const userController = require("../../logic/user/user.controller")
const userImageController = require("../../logic/user/user.image.controller")


const uc = new userController();
const uic = new userImageController();

router.post("/register", validateFrmLoginMiddleware, uc.register)
router.post("/email/resend", uc.resend)
router.get("/profile", AUTHmiddleware("user"), uc.detailUser)
router.put("/profile", AUTHmiddleware("user"), uc.editUser)
router.post("/avatar", AUTHmiddleware("user"), uic.saveAvatar)
router.post("/idcard", AUTHmiddleware("user"), uic.saveIdcard)


module.exports = router;