const express = require("express");
const router = express.Router();

const NewsController = require("../../logic/news/news.controller");
const news = new NewsController();

const NewsImageController = require("../../logic/news/news.image.controller");
const newsThumbnail = new NewsImageController();

router.get("/list", news.list);
router.get("/featured", news.featured);
router.get("/detail/:id", news.detail);
router.post("/thumbnail/:id", newsThumbnail.saveThumbnail)

module.exports = router;
