const express = require('express');
const userController = require("../../logic/user/user.controller");
const paymentRedirController = require("../../logic/payment/payment.redirect.controller");

const router = express.Router();

const uc = new userController();
const pr = new paymentRedirController();

router.get("/user/activate/:id", uc.activate);
router.get("/user/reset/password/:id", uc.resetForm);
router.post("/user/reset/password/:id", uc.resetSave);

router.get("/payment/finish", pr.finish);
router.get("/payment/qrnotify", pr.finishqr);
router.post("/payment/notify", pr.notify);

module.exports = router;