const express = require("express");
const router = express.Router();

const { apikeyMiddleware } = require("../middleware/APIKEY.middleware");
router.use(apikeyMiddleware);

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")

const userController = require("../../logic/user/user.controller")
const merchantController = require("../../logic/merchant/merchant.controller");

const user = new userController();
const merchant = new merchantController();

router.post("/register", merchant.register)

router.get("/product", AUTHmiddleware("merchant"), merchant.listProduct)
router.get("/order", AUTHmiddleware("merchant"), merchant.listOrder)
router.get("/order/:id", AUTHmiddleware("merchant"), merchant.listOrderDetail)
router.get("/profile", AUTHmiddleware("merchant"), merchant.profile)

router.put("/accept/:id", AUTHmiddleware("merchant"), merchant.accept)
router.put("/discard/:id", AUTHmiddleware("merchant"), merchant.discard)
router.put("/shipping/:id", AUTHmiddleware("merchant"), merchant.shipping)
router.put("/edituser/:id", AUTHmiddleware("merchant"), user.editUser);

module.exports = router;