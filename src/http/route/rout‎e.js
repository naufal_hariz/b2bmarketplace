const express = require("express");
const router = express.Router();

const userRoute = require("./userRoute");
router.use("/user", userRoute);

const authRoute = require("./authRoute");
router.use("/auth", authRoute);

const dataSupportRoute = require("./dataSupportRoute");
router.use("/data", dataSupportRoute);

const productRoute = require("./productRoute");
router.use("/product", productRoute);

const loanRoute = require("./loanRoute");
router.use("/loan", loanRoute);

const paymentRoute = require("./paymentRoute");
router.use("/payment", paymentRoute);

const newsRoute = require("./newsRoute");
router.use("/news", newsRoute);

const topupRoute = require("./topupRoute");
router.use("/topup", topupRoute);

const walletRoute = require("./walletRoute");
router.use("/wallet", walletRoute);

const merchantRoute = require("./merchantRoute");
router.use("/merchant", merchantRoute);

const adminRoute = require("./adminRoute");
router.use("/admin", adminRoute);

const cartRoute = require("./cartRoute");
router.use("/cart", cartRoute);

const orderRoute = require("./orderRoute");
router.use("/order", orderRoute);

module.exports = router;
