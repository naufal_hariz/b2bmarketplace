const validateInput = require('../../helper/validate.helper');
const { Sequelize, City, User } = require("../../models");


module.exports = {
    validateFrmLoginMiddleware: async (req, res, next) => {

        let { email, city, province } = req.body

        let notNull = ['name', 'password', 'phone', 'address']
        let number = ['province', 'city']

        const error = {};

        let vldtEmail = validateInput.email(email)
        if (!vldtEmail) error['email'] = "format not valid"

        notNull.forEach(e => {
            if (!validateInput.notNull(req.body[e])) error[e] = `${e} can't be null`
        });

        number.forEach(e => {
            if (!validateInput.numberInteger(req.body[e])) error[e] = `${e} must be number`
        });

        if ((Object.keys(error).length === 0 && error.constructor === Object)) {
            let ValidateCity = await City.findOne({ where: { id: city, provinceId:province} })
            if(!ValidateCity) error['city'] = "Wrong City / province data id"

            let userExist = await User.findOne({where:{email}})
            if(userExist) error['email'] = "Email already registered"
        }

        if (!(Object.keys(error).length === 0 && error.constructor === Object)) {

            let msg = {
                "code": 422,
                "message": "Attempt Register Failed",
                "entity": "validation",
                "state": "attemptRegistrationValidationFailed",
                "error": error
            }
            res.status(422).send(msg)
            return
        }

        next()
    }
}