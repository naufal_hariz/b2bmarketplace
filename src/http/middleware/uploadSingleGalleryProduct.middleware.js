const util = require("util");
const path = require("path");
const multer = require("multer");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, path.join(`${__dirname}/../../../public/upload/product`));
  },

  filename: (req, file, callback) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg`;
      return callback(message, null);
    }

    var filename = `${Date.now()}.jpg`;
    callback(null, filename);
  },
});

var upload = multer({ storage }).single("photo");
var uploadSingle = util.promisify(upload);

module.exports = uploadSingle;
