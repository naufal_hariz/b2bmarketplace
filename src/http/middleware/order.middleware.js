const responseHelper = require('../../helper/response.helper');

module.exports = {
    orderMiddleware: async (req, res, next) => {
        try {
            const { merchantId, total, dataProduct, shipping } = req.body
            if (merchantId == undefined || total == undefined || dataProduct == undefined || shipping == undefined 
                || merchantId == "" || total == "" || dataProduct == "" || shipping == "") {
                    const bodyRes = {
                        "code": 401,
                        "api": "Order",
                        "message": "Coloumn Not Complete or Empty",
                        "entity": "Order",
                        "state": "OrderFailed"
                    }
                    res.status(401).send(bodyRes)
                    return
            } else {
                next()
            }
        } catch(error) {
            console.log(error)
            const bodyRes = {
                "code": 500,
                "api": "Register",
                "message": "Internal Server Error",
                "entity": "User",
                "state": "submitOrderFailed"
            }
            res.status(500).send(bodyRes)
            return
        }
    } 
};