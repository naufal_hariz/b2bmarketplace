module.exports = {
    apikeyMiddleware: (req, res, next) => {
        try {
            const apiKey = req.header('x-api-key')
            if (apiKey !== process.env.SECRET) {
                res.status(401).send({
                    code: 401,
                    message: "API Key not found / valid",
                    entity: "x-api-key",
                    state: "error",
                })
                return
            }
            else {
                next()
            }
        }
        catch (error) {
            console.log(error)

            res.status(500).send({
                code: 500,
                message: "Internal Server Error",
                entity: "x-api-key",
                state: "error",
            })
            return
        }
    }
};
