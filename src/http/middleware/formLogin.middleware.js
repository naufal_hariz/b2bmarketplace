const validateInput = require('../../helper/validate.helper');

module.exports = {
    validateFrmLoginMiddleware: (req, res, next) => {
        let { email, password } = req.body

        let vldtEmail = validateInput.email(email)
        let vldtPassword = validateInput.notNull(password)

        const error = [];

        if (!vldtPassword) error.push({password: "Password can't be null" });
        if (!vldtEmail) error.push({ Email : "Email format not valid" });

        if (error.length != 0) {

            let msg = {
                "code": 422,
                "message": "Attempt Login Failed",
                "entity": "validation",
                "state": "attemptValidationFailed",
                "error": error
            }
            res.status(422).send(msg)
            return
        }

        next()
        return
    }
};
