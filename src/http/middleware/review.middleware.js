const responseHelper = require('../../helper/response.helper');
module.exports = {
    reviewMiddleware: async (req, res, next) => {
        try {
            const { title, rating, content} = req.body
            if (title == undefined || rating == undefined || content == undefined 
                || title == "" || rating == "" || content == "") {
                    const bodyRes = {
                        "code": 401,
                        "api": "Review",
                        "message": "Coloumn Not Complete or Empty",
                        "entity": "Review",
                        "state": "reviewFailed"
                    }
                    res.status(401).send(bodyRes)
                    return
            } else {
                next()
            }
        } catch(error) {
            console.log(error)
            const bodyRes = {
                "code": 500,
                "api": "Register",
                "message": "Internal Server Error",
                "entity": "User",
                "state": "createUserFailed"
            }
            res.status(500).send(bodyRes)
            return
        }
    } 
};