const logHelper = require('../../helper/log.helper');

module.exports = async function logMiddleware(req, res, next) {
        try {
            const method = req.method
            const url = req.url
            const userAgent = req.header('user-agent')
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
            var reqData = { header: {}, body: {} }
            var resData = {}

            if (req.headers) {
                reqData.header = req.headers;
            } 
            if (req.body) {
                reqData.body = req.body;
            }
            if (reqData.body.password){
                reqData.body.password = '**********'
            }
            resData = req.businessLogic;

            reqData = JSON.stringify(reqData)
            resData = JSON.stringify(resData)
            const logData = {
                "method": method,
                "url": url,
                "userAgent": userAgent,
                "ip": ip,
                reqData,
                resData
            }
            const result = logHelper.storeLog(logData)
            next()


        } catch(error) {
            console.log(error)
            const bodyRes = {
                message: "Internal Server Error"
            }
            res.status(500).send(bodyRes)
            return
        }
    }
