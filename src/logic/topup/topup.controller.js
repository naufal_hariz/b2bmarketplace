const responseHelper = require("../../helper/response.helper");
const { sequelize, Topup, Wallet, User } = require("../../models");
const { v4: uuidv4 } = require("uuid");

class TopupController {
  async submitManualTopup(req, res, next) {
    const customerUID = req.user.uid;
    const transactionTime = new Date();
    const nominal = req.body.nominal;
    const dataTopup = {
      id: uuidv4(),
      customerId: customerUID,
      paymentTime: transactionTime,
      paymentMethod: "Transfer Bank",
      paymentTo: "Wallet",
      paymentNominal: nominal,
      paymentProof: "", //sementara kosong dulu deh, langsung approve juga ini
    };

    const dataWallet = {
      customerId: customerUID,
      transactionTime: transactionTime,
      description: "TopUp Manual Transfer",
      type: "Top Up",
      nominal: nominal,
    };

    const user = await User.findByPk(dataTopup.customerId);
    if (user && user.id) {
      let transaction = await sequelize.transaction();
      await user.update(
        { cashBalance: user.cashBalance + dataTopup.paymentNominal },
        { transaction }
      );
      const topup = await Topup.create(dataTopup, { transaction });
      const wallet = await Wallet.create(dataWallet, { transaction });
      await transaction.commit();

      if (topup && topup.id) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Submit Manual Topup Success!!`,
          entity: "topup",
          state: "SuccessSubmitTopup",
          data: { topup, wallet, user },
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, submit topup failed`,
          entity: "topup",
          state: "FailedSubmitManualTopup",
        });
      }
    } else {
      req.businessLogic = await responseHelper({
        code: 404,
        message: `Sorry, submit topup failed`,
        entity: "topup",
        state: "FailedSubmitManualTopup",
      });
    }
    next();
    return;
  }
}

module.exports = TopupController;
