const responseHelper = require('../../helper/response.helper');
const authHelper = require('../../helper/auth.helper');
const { Merchant } = require("../../models");
const bcrypt = require('bcryptjs');

const jwt = require('jsonwebtoken');

class AuthController {
    async auth(req, res, next) {
        try {
            const { email, password } = req.body
            const findUser = await authHelper.getUser(email)

            if (findUser) {
                const validPassword = await bcrypt.compare(password, findUser.result.password);
                let isAdmin = (findUser.result.isAdminB2B) ? findUser.result.isAdminB2B : false
                let isMerchant = (findUser.result.isAdminMerchant) ? findUser.result.isAdminMerchant : false

                if (isMerchant) {
                    let Merch = await Merchant.findOne({ where: { adminMerchantId: findUser.result.id } })
                    if (Merch.verificationStatus !== "ACTIVE")
                        throw "Merchant not active"
                }

                if(!(isAdmin & isMerchant)){
                    if(!findUser.result.isActive) throw "User not activate, please check email to activated"
                }

                if (validPassword) {

                    const token = jwt.sign({
                        uid: findUser.result.id
                    }, process.env.SECRET, { expiresIn: '24h' });

                    req.businessLogic = await responseHelper({
                        "code": 200,
                        "message": "Attempt Authentication Success",
                        "entity": "authentication",
                        "state": "attemptAuthenticationSuccess",
                        "data": {
                            "token": token,
                            isAdmin,
                            isMerchant
                        }
                    })
                    next()
                    return

                } else {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "message": "Username or password invalid",
                        "entity": "authentication",
                        "state": "attemptAuthenticationError",
                    })
                    next()
                    return
                }
            }

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": error,
                "entity": "attemptAuthenticationError",
                "state": "attemptAuthenticationError",
                error
            })
            next()
            return
        }
    }
}

module.exports = AuthController