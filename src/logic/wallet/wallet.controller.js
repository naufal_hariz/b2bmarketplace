const responseHelper = require("../../helper/response.helper");
const authHelper = require('../../helper/auth.helper');
const { Sequelize, Wallet } = require("../../models");
const Op = Sequelize.Op;

class WalletController {
    async balance(req, res, next){
        try{
            const getProfile = await authHelper.getUserById(req.user.uid)
            if (getProfile.success) {
                req.businessLogic = await responseHelper({
                    code: 200,
                    message: "Get Wallet Success",
                    entity: "wallet",
                    state: "getWalletSuccess",
                    data: {
                        name: getProfile.result.name,
                        cashBalance: getProfile.result.cashBalance
                    }
                })
            } else {
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "message": "Uauthorized",
                    "entity": "wallet",
                    "state": "Unauthorized"
                })
            }
            next()
            return
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "wallet",
                "state": "getWalletError",
            })
            next()
            return
        }
    }

    async history(req, res, next){
        try{
            const walletCount = await Wallet.count({ where: { customerId: req.user.uid }})
            if (walletCount > 0) {
                const walletHistory = await Wallet.findAll({where: { customerId: req.user.uid }})
                req.businessLogic = await responseHelper({
                    code: 200,
                    message: "Get Wallet History Success",
                    entity: "Wallet",
                    state: "getWallethistorySuccess",
                    data: walletHistory
                })
            }
            else {
                req.businessLogic = await responseHelper({
                    code: 404,
                    message: "Sorry, no transaction found",
                    entity: "Wallet",
                    state: "getWallethistorySuccess"
                })
            }
            next()
            return
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "wallet",
                "state": "getWallethistorySError",
            })
            next()
            return
        }
    }
}

module.exports = WalletController