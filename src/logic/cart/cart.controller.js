const responseHelper = require('../../helper/response.helper');
const { sequelize, Sequelize , Cart, MerchantProduct } = require("../../models");
const Op = Sequelize.Op;

class CartController {

    async show(req, res, next) {

        try {
            let data = await Cart.findAll(
                {
                    where: { userId: req.user.uid },
                    include: ["product", "merchant"]
                }
            )
            let cart = {}, cartData = [], totalQty = 0, totalAmount = 0, weightTotal = 0
            data.forEach(e => {
                if (cart[e.merchantId] === undefined)
                    cart[e.merchantId] = []
                cart[e.merchantId].push(e)
            });

            for (const property in cart) {
                let arr = []
                let merchantId, merchatnName, merchantTotal = 0

                cart[property].forEach(ee => {
                    console.log("ee", ee)
                    let total = ee.product.price * ee.qty
                    merchantId = ee.merchantId
                    merchatnName = ee.merchant.name
                    merchantTotal += total
                    let totalWeight = ee.product.weight * ee.qty
                    weightTotal += totalWeight
                    arr.push({
                        productId: ee.product.id,
                        product: ee.product.name,
                        price: ee.product.price,
                        banner: ee.product.banner,
                        qty: ee.qty,
                        total: total,
                        weight: ee.product.weight,
                        totalWeight,
                        isChecked: (ee.isChecked == true) ? true : false
                    })
                    totalQty += ee.qty
                    totalAmount += total
                });
                cartData.push({ merchantId, merchatnName, merchantTotal, items: arr })
            }


            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "show to cart Success",
                "entity": "Cart",
                "state": "ShowCart",
                "data": {
                    total: {
                        items: totalQty,
                        merchant: cartData.length,
                        amount: totalAmount,
                        weight: weightTotal
                    },
                    cart: cartData
                }
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "message": "show to cart Success",
                "entity": "Cart",
                "state": "ShowCart",
                "error": error.toString
            })
        }


        next()
        return
    }

    async save(req, res, next) {
        const userId = req.user.uid

        try {

            let { productId, qty, merchantId } = req.body

            if (!(await MerchantProduct.findOne({ where: { productId, merchantId, } })))
                throw "Product not exist on merchant ID"

            if (await Cart.findOne({ where: { userId, productId, merchantId } }))
                await Cart.destroy({ where: { userId, productId, merchantId } })

            await Cart.create({ userId, productId, qty, merchantId })
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "add to cart Success",
                "entity": "Cart",
                "state": "AddCart",
                "data": req.body
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "Cart",
                "state": "AddCart",
                "error": error
            })
        }

        next()
        return
    }

    async delete(req, res, next) {
        const userId = req.user.uid

        try {
            let { productId, merchantId } = req.body

            if (!(await Cart.findOne({ where: { productId, merchantId, userId } })))
                throw "Product not found on cart"

            await Cart.destroy({ where: { userId, productId, merchantId } })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "delete cart Success",
                "entity": "Cart",
                "state": "DeleteCart",
                "data": ""
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "delete cart failed",
                "entity": "Cart",
                "state": "deleteCartFailed",
                "error": error
            })
        }

        next()
        return
    }

    async deleteAll(req, res, next) {
        const userId = req.user.uid

        try {
            if (!(await Cart.findOne({ where: { userId } })))
                throw "Cart empty"

            await Cart.destroy({ where: { userId } })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "delete cart Success",
                "entity": "Cart",
                "state": "DeleteCart",
                "data": ""
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "delete cart failed",
                "entity": "Cart",
                "state": "deleteAllCartFailed",
                "error": error
            })
        }

        next()
        return
    }

    async check(req, res, next) {
        try {
            let { merchantId, productId, isChecked } = req.body

            isChecked = (isChecked) ? true : false

            let transaction = await sequelize.transaction();
            await Cart.update(
                { isChecked },
                { where: { merchantId, productId } },
                { transaction }
            )

            await Cart.update(
                { isChecked: false },
                { where: { merchantId: { [Op.ne]: merchantId } } },
                { transaction }
            )

            await transaction.commit();

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "check item on cart Success",
                "entity": "Cart",
                "state": "UpdateCart",
                "data": req.body
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "check item on cart Failed",
                "entity": "Cart",
                "state": "UpdateCartFailed",
                "data": error
            })
        }

        next()
        return
    }

}

module.exports = CartController