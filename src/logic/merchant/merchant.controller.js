const responseHelper = require('../../helper/response.helper');
const { Merchant, User, Order, OrderDetail, MerchantProduct, sequelize, OrderWorkflow, Cart } = require("../../models");
const bcrypt = require('bcryptjs');
const { v4: uuidv4 } = require("uuid");
const emailTransactionHelper = require('../../helper/emailTransaction.helper');

class MerchantController {

    async list(req, res, next) {
        let data = await Merchant.findAll({
            include: ["city", "province", "user"],
            order: [['createdAt', 'ASC']]
        })
        req.businessLogic = await responseHelper({
            "code": 200,
            "message": "Get Merchants Success",
            "entity": "Merchant",
            "state": "getMerchantsSuccess",
            "data": data
        });
        next();
        return;
    }

    async detail(req, res, next) {
        const { id } = req.params;
        let data = await Merchant.findAll({
            where: { id },
            include: ["city", "province", "products"]
        })

        if (data) {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": `Retrieve merchant with id ${id} success!!`,
                "entity": "merchant",
                "state": "getMerchantDetail",
                "data": data,
            });
        } else {
            req.businessLogic = await responseHelper({
                "code": 404,
                "message": `Merchant with id ${id} not found!!`,
                "entity": "merchant",
                "state": "getMerchantDetail",
            });
        }
        next();
        return;
    }

    async profile(req, res, next) {
        const id = req.user.uid;
        let data = await User.findOne({
            where: { id },
            include: "merchants",
        })

        if (data) {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": `Retrieve merchant with id success!!`,
                "entity": "merchant",
                "state": "getMerchantProfile",
                "data": data,
            });
        } else {
            req.businessLogic = await responseHelper({
                "code": 404,
                "message": `Merchant not found!!`,
                "entity": "merchant",
                "state": "getMerchantProfile",
            });
        }
        next();
        return;
    }

    async active(req, res, next) {
        const data = await Merchant.findAll({
            where: {
                verificationStatus: {
                    [Op.eq]: true
                }
            }
        });
        if (data.verificationStatus) {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": `Retrieve merchant with status ${verificationStatus} success!!`,
                "entity": "merchantStatus",
                "state": "getMerchantStatus",
                "data": data,
            });
        } else {
            req.businessLogic = await responseHelper({
                "code": 404,
                "message": `Merchant with status ${verificationStatus} not found!!`,
                "entity": "merchantStatus",
                "state": "getMerchantStatus",
            });
        }
        next();
        return;
    }

    async enable(req, res, next) {
        try {
            const { id } = req.params;

            if (await Merchant.update({ verificationStatus: "ACTIVE" }, { where: { id } })) {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "enable merchant success",
                    "entity": "enableMerchant",
                    "state": "enableMerchantSUccess",
                    "data": {},
                })
            }

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "enableMerchant",
                "state": "enableMerchantError",
                "message": "Merchant not found",
            })
        }
        next()
        return
    }

    async disable(req, res, next) {
        try {
            const { id } = req.params;

            if (await Merchant.update({ verificationStatus: "INACTIVE" }, { where: { id } })) {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "disable merchant success",
                    "entity": "disableMerchant",
                    "state": "disableMerchantSUccess",
                    "data": {},
                })
            }

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "disableMerchant",
                "state": "disableMerchantError",
                "message": "Merchant not found",
            })
        }
        next()
        return
    }

    async delete(req, res, next) {
        try {
            let { id } = req.params

            let merch = await Merchant.findOne({ where: { id } })
            let userId = merch.adminMerchantId

            if (await Order.findOne({ where: { merchantId: id } })) throw "Merchant has orders, try non active instead of deletion"
            if (await Cart.findOne({ where: { merchantId: id } })) throw "Merchant has active cart, try non active instead of deletion"
            if (await MerchantProduct.findOne({ where: { merchantId: id } })) throw "Merchant has active products, try non active instead of deletion"

            await Merchant.destroy({ where: { id } });
            await User.destroy({ where: { id: userId } })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "delete success",
                "entity": "deleteMerchant",
                "state": "deleteMerchantSUccess",
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "deleteMerchant",
                "state": "deleteMerchantError",
                "error": error
            })
        }
        next()
        return
    }

    async register(req, res, next) {
        try {
            let dataUser = req.body
            let { email, name } = req.body
            dataUser['cityId'] = req.body.city
            dataUser['provinceId'] = req.body.province
            dataUser['password'] = bcrypt.hashSync(req.body.password, 10)
            dataUser['isAdminMerchant'] = true
            dataUser['isActive'] = true

            if (await User.findOne({ where: { email } })) throw "Email already registered"

            let userCreated = await User.create(dataUser)

            let merchantData = {
                adminMerchantId: userCreated.id,
                name: dataUser.merchantName,
                provinceId: dataUser.province,
                cityId: dataUser.city,
                photo: "https://dzahin-storage.s3-ap-southeast-1.amazonaws.com/tobiko/merchant/logo-kios.jpg",
                verificationStatus: "PENDING"
            }
            await Merchant.create(merchantData)

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Registration success, your merchant status waiting approval from admin",
                "entity": "Registration",
                "state": "userRegistrationSuccess",
                "data": {
                    email, name,
                    merchantName: dataUser.merchantName
                }
            })
        } catch (error) {
            console.log(error)

            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "Registration Failed",
                "entity": "Registration",
                "state": "userRegistrationFailed",
                "error": error
            })
        }
        next()
        return
    }

    async listProduct(req, res, next) {
        try {
            const merchantId = req.merchant.id

            let data = await MerchantProduct.findAll(
                {
                    where: { merchantId },
                    include: ["product"]
                }
            )

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Merchants Product Success",
                "entity": "MerchantProduct",
                "state": "getMerchantsSuccess",
                "data": data
            });

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "message": "Get Merchants Product Failed",
                "entity": "MerchantProduct",
                "state": "getMerchantsProductFailed",
                "error": error
            })
        }

        next();
        return;
    }

    async listOrder(req, res, next) {
        try {
            const merchantId = req.merchant.id

            let data = await Order.findAll(
                {
                    attributes: [
                        'id', 'merchantId', 'invoiceNumber', 'paymentNominal',
                        'shippingPartner', 'shippingService', 'shippingTrackingNumber',
                        'paymentMethod', 'paymentStatus', 'createdAt', 'totalAmount'
                    ],
                    where: { merchantId },
                    include: ["customer", "workflow"],
                    order: [['createdAt', 'DESC']]
                }
            )

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Merchants Order Success",
                "entity": "MerchantOrder",
                "state": "getMerchantOrderSuccess",
                "data": data
            });

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "message": "Get Merchants Order Failed",
                "entity": "MerchantOrder",
                "state": "getMerchantOrderFailed",
                "error": error
            })
        }

        next();
        return;
    }

    async listOrderDetail(req, res, next) {
        try {
            const merchantId = req.merchant.id
            const orderId = req.params.id

            let data = await Order.findOne(
                {
                    attributes: ['id', 'merchantId', 'invoiceNumber', 'paymentNominal', 'shippingTrackingNumber', 'totalAmount'],
                    where: { merchantId, id: orderId },
                    include: ["customer", "workflow"],

                }
            )

            let orderDetail = await OrderDetail.findAll({
                where: { orderId },
                include: ["product"]
            })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Order Detail Success",
                "entity": "OrderDetail",
                "state": "getOrderDetailSuccess",
                "data": {
                    order: data,
                    items: orderDetail
                }
            });

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "message": "Get Order Detail Failed",
                "entity": "OrderDetail",
                "state": "getOrderDetailFailed",
                "error": error
            })
        }

        next();
        return;
    }

    async accept(req, res, next) {
        try {
            const { id } = req.params;
            let workflowId = 3 //order progress

            let transaction = await sequelize.transaction();
            await Order.update({ workflowId }, { where: { id } }, { transaction })
            await OrderWorkflow.create({
                id: uuidv4(),
                orderId: id,
                workflowTime: new Date,
                workflowId,
            }, { transaction })
            await transaction.commit()

            let order = await Order.findOne({ where: { id }, include: ["customer", "workflow"] })
            emailTransactionHelper.sendOrderNotif(order)

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "accept order success",
                "entity": "orderStatus",
                "state": "success",
                "data": await OrderWorkflow.findAll({ where: { orderId: id }, include: ['workflow'] })
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "orderStatus",
                "state": "Error",
                "message": "order not found",
            })
        }
        next()
        return
    }

    async discard(req, res, next) {
        try {
            const { id } = req.params;
            let workflowId = 7 //order discard

            let transaction = await sequelize.transaction();
            await Order.update({ workflowId }, { where: { id } }, { transaction })
            await OrderWorkflow.create({
                id: uuidv4(),
                orderId: id,
                workflowTime: new Date,
                workflowId,
            }, { transaction })
            await transaction.commit();

            let order = await Order.findOne({ where: { id }, include: ["customer", "workflow"] })
            emailTransactionHelper.sendOrderNotif(order)

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "accept order success",
                "entity": "orderStatus",
                "state": "success",
                "data": await OrderWorkflow.findAll({ where: { orderId: id }, include: ['workflow'] }),
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "orderStatus",
                "state": "Error",
                "message": "order not found",
            })
        }
        next()
        return
    }

    async shipping(req, res, next) {
        try {
            const { id } = req.params;
            const shippingNumber = req.body.shippingTrackingNumber
            let workflowId = 4

            if (!Order.findByPk(id)) throw "order not found"

            let transaction = await sequelize.transaction();
            await Order.update(
                { shippingTrackingNumber: shippingNumber, workflowId },
                { where: { id } },
                { transaction }
            )
            await OrderWorkflow.create({
                id: uuidv4(),
                orderId: id,
                workflowTime: new Date,
                workflowId
            }, { transaction })
            await transaction.commit();

            let order = await Order.findOne({ where: { id }, include: ["customer", "workflow"] })
            emailTransactionHelper.sendOrderNotif(order)

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "update invoice success",
                "entity": "updateInvoice",
                "state": "success",
                "data": await OrderWorkflow.findAll({ where: { orderId: id }, include: ['workflow'] }),
            })

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "updateInvoice",
                "state": "Error",
                "message": "order not found",
            })

        }
        next();
        return;
    }

}

module.exports = MerchantController