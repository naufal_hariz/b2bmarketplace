const responseHelper = require('../../helper/response.helper');
const { News, Sequelize } = require("../../models");
const Op = Sequelize.Op;

class newsController {

    async list(req, res, next) {
        const limit = 10;
        const page = typeof req.query.page === "undefined" ? 1 : +req.query.page;
        const newsCount = await News.count();
        const totalPages = Math.ceil(newsCount / limit);

        const news = await News.findAll({
        limit,
        offset: (page - 1) * limit,
        });

        if (newsCount > 0) {
        req.businessLogic = await responseHelper({
            code: 200,
            message: `Retrieve all news in page ${page} success!!`,
            entity: "news",
            state: "getAllListNews",
            data: {
            page,
            total_pages: totalPages,
            total_results: newsCount,
            total_data: news.length,
            news,
            },
        });
        } else {
        req.businessLogic = await responseHelper({
            code: 404,
            message: `Sorry, news still empty!!`,
            entity: "news",
            state: "getAllListNews",
        });
        }
        next();
        return;
    }

    async detail(req, res, next) {
        const { id } = req.params;
        const news = await News.findByPk(id);
        if (news && news.id) {
            req.businessLogic = await responseHelper({
                code: 200,
                message: `Retrieve news with id ${id} success!!`,
                entity: "news",
                state: "getNewsDetail",
                data: news,
            });
        } else {
            req.businessLogic = await responseHelper({
                code: 404,
                message: `News with id ${id} not found!!`,
                entity: "news",
                state: "getNewsDetail",
            });
        }
        next();
        return;
    }

    async featured(req, res, next) {
        const news = await News.findAll({
        where: {isFeatured: true}
        });

        if (news.length > 0) {
        req.businessLogic = await responseHelper({
            code: 200,
            message: `Retrieve all featured news success!!`,
            entity: "news",
            state: "getAllFeaturedNews",
            data: news
        });
        } else {
        req.businessLogic = await responseHelper({
            code: 404,
            message: `Sorry, featured news still empty!!`,
            entity: "news",
            state: "getAllFeaturedNews",
        });
        }
        next();
        return;
    }

}

module.exports = newsController