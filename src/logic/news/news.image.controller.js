const responseHelper = require('../../helper/response.helper');
const { News } = require("../../models");
const uploadHelper = require("../../helper/upload.helper")

class newsImageController {

    async saveThumbnail(req, res, next) {

        try {
            if (!req.body.image) throw "Body image base64 not valid"

            let uplHandler = await uploadHelper.base64(req.body.image)
            if (!uplHandler.success) throw "Failed uploading"

            let image = await uploadHelper.awsStorage({ file: uplHandler.file.image, folder: "thumbnail" })
            let thumb = await uploadHelper.awsStorage({ file: uplHandler.file.thumb, folder: "thumbnail", thumb: true })
            console.log(req.params)
            console.log(image)
            await News.update({
                thumbnail: image
            }, {
                where: {id: req.params.id}
            })

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": "Upload Image Success",
                "entity": "News",
                "state": "setNewsThumbnailSuccess",
                "data": {
                    thumbnail: image,
                    thumb
                }
            })
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "Upload Image Error",
                "entity": "News",
                "state": "setNewsThumbnailFailed",
                "error": {
                    error
                }
            })
        }
        next()
        return
    }
}
module.exports = newsImageController