const responseHelper = require("../../helper/response.helper");
const {
  sequelize,
  User,
  Loan,
  LoanPayment,
  Order,
  OrderWorkflow,
} = require("../../models");
const { v4: uuidv4 } = require("uuid");

class LoanController {
  async list(req, res, next) {
    try {
      let data = await Loan.findAll({
        include: "customer",
        order: [["createdAt", "DESC"]],
      });

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Get All Loan Data Success",
        entity: "Loan",
        state: "getLoanSuccess",
        data: data,
      });
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "loan",
        state: "FailedGetLoan",
        error: error,
      });
    }
    next();
    return;
  }

  async submitLoan(req, res, next) {
    try {
      const customerUID = req.user.uid;
      const dataLoan = {
        id: uuidv4(),
        customerId: customerUID,
        submittedTime: new Date(),
        orderId: req.body.orderId,
        nominal: req.body.nominal,
        tenor: req.body.tenor || 1,
        installment: Math.ceil(+req.body.nominal / +req.body.tenor),
        currentBalance: 0,
        totalPaid: 0,
        status: "WAITING-APPROVAL",
      };
      const loan = await Loan.create(dataLoan);

      if (loan && loan.id) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: "Submit Loan Success!!",
          entity: "loan",
          state: "SuccessSubmitLoan",
          data: loan,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: "Sorry, submit loan failed",
          entity: "loan",
          state: "FailedSubmitLoan",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "loan",
        state: "FailedSubmitLoan",
        error: error,
      });
    }
    next();
    return;
  }

  async approveLoan(req, res, next) {
    const { id } = req.params;
    try {
      const adminUID = req.user.uid;
      const responseTime = new Date();

      const loan = await Loan.findByPk(id, {
        include: {
          model: User,
          as: "customer",
          attributes: ["id", "name", "email", "loanBalance"],
        },
      });

      if (loan && loan.id && loan.customer.id) {
        const user = await loan.getCustomer();
        const tenor = loan.tenor;
        let dueDate = new Date(responseTime);
        dueDate.setMonth(dueDate.getMonth() + +tenor);

        const dataApproval = {
          dueDate,
          currentBalance: loan.nominal,
          responseStatus: "APPROVED",
          responseTime,
          responseBy: adminUID,
          verificationNotes: req.body.notes || "",
          status: "APPROVED",
          customer: {
            id: loan.customer.id,
            name: loan.customer.name,
            email: loan.customer.email,
            loanBalance: +loan.customer.loanBalance + +loan.nominal,
          },
        };

        let transaction = await sequelize.transaction();
        await user.update(
          { loanBalance: +user.loanBalance + +loan.nominal },
          { transaction }
        );
        await loan.update(dataApproval, { transaction });
        await Order.update(
          {
            paymentMethod: "LOAN",
            paymentTo: "LOAN",
            paymentTime: new Date(),
            workflowId: 2,
          },
          { where: { id: loan.orderId } },
          { transaction }
        );
        await OrderWorkflow.create(
          {
            id: uuidv4(),
            orderId: loan.orderId,
            workflowTime: new Date(),
            workflowId: 2,
          },
          { transaction }
        );
        await transaction.commit();

        req.businessLogic = await responseHelper({
          code: 200,
          message: "Approve Loan Success!!",
          entity: "loan",
          state: "SuccessApproveLoan",
          data: loan,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, approve loan with id ${id} failed`,
          entity: "loan",
          state: "FailedApproveLoan",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: `Sorry, approve loan with id ${id} failed`,
        entity: "loan",
        state: "FailedApproveLoan",
        error: error,
      });
    }

    next();
    return;
  }

  async rejectLoan(req, res, next) {
    const { id } = req.params;
    try {
      const adminUID = req.user.uid;
      const responseTime = new Date();

      const loan = await Loan.findByPk(id, {
        include: {
          model: User,
          as: "customer",
          attributes: ["id", "name", "email", "loanBalance"],
        },
      });

      if (loan && loan.id && loan.customer.id) {
        const dataApproval = {
          responseStatus: "REJECT",
          responseTime,
          responseBy: adminUID,
          verificationNotes: req.body.notes || "",
          status: "REJECT",
        };

        let transaction = await sequelize.transaction();
        await loan.update(dataApproval, { transaction });
        await transaction.commit();

        req.businessLogic = await responseHelper({
          code: 200,
          message: "Reject Loan Success!!",
          entity: "loan",
          state: "SuccessApproveLoan",
          data: loan,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, reject loan with id ${id} failed`,
          entity: "loan",
          state: "FailedRejectLoan",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: `Sorry, reject loan with id ${id} failed`,
        entity: "loan",
        state: "FailedRejectLoan",
        error: error,
      });
    }
    next();
    return;
  }

  async summaryLoan(req, res, next) {
    try {
      const customerUID = req.user.uid;
      const loans = await Loan.findAll({
        include: [
          {
            model: User,
            as: "customer",
            attributes: ["id", "name", "email"],
          },
          {
            model: LoanPayment,
            as: "loanPayments",
            attributes: [
              "id",
              "paymentTime",
              "paymentMethod",
              "paymentTo",
              "paymentNominal",
            ],
          },
        ],
        where: { customerId: customerUID },
        order: [["createdAt", "DESC"]],
      });

      if (loans.length > 0) {
        let summaryLoan = [];
        loans.forEach((loan) => {
          const approvalTime = loan.responseTime;
          let nextDueDate = new Date(approvalTime);
          nextDueDate.setMonth(
            nextDueDate.getMonth() + loan.loanPayments.length + 1
          );
          if (loan.status === "APPROVED") {
            const nextPayment = {
              nextDueDate,
              nextInvoice: loan.installment,
            };

            summaryLoan.push({
              loan,
              nextPayment,
            });
          } else {
            summaryLoan.push({
              loan,
            });
          }
        });

        req.businessLogic = await responseHelper({
          code: 200,
          message: "Get Summary Loan Success!!",
          entity: "loan",
          state: "SuccessGetSummaryLoan",
          data: summaryLoan,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 200,
          message: "Get Summary Loan Success even still Empty!!",
          entity: "loan",
          state: "SuccessGetEmptySummaryLoan",
          data: loans,
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, get summary loan failed",
        entity: "loan",
        state: "FailedGetSummaryLoan",
        error: error,
      });
    }
    next();
    return;
  }

  async nextPaymentLoan(req, res, next) {
    try {
      const customerUID = req.user.uid;
      const loan = await Loan.findOne({
        where: { customerId: customerUID, status: "APPROVED" },
      });

      if (loan && loan.id) {
        const loanPayments = await LoanPayment.findAndCountAll({
          where: { loanId: loan.id },
        });
        const approvalTime = loan.responseTime;
        let nextDueDate = new Date(approvalTime);
        nextDueDate.setMonth(nextDueDate.getMonth() + loanPayments.count + 1);
        const nextPayment = {
          nextDueDate,
          nextInvoice: loan.installment,
        };

        req.businessLogic = await responseHelper({
          code: 200,
          message: "Get Next Payment Loan Success!!",
          entity: "loan",
          state: "SuccessGetNextPaymentLoan",
          data: nextPayment,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: "Sorry, get next payment loan failed",
          entity: "loan",
          state: "FailedGetNextPaymentLoan",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, get next payment loan failed",
        entity: "loan",
        state: "FailedGetNextPaymentLoan",
        error: error,
      });
    }
    next();
    return;
  }

  async historyPayment(req, res, next) {
    try {
      const { loanId } = req.params;
      const loanPayments = await LoanPayment.findAll({
        where: { loanId },
      });

      if (loanPayments.length > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: "Get History Loan Payment Success!!",
          entity: "loan",
          state: "SuccessGetHistoryLoanPayment",
          data: loanPayments,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: "Sorry, get history loan payment not found",
          entity: "loan",
          state: "FailedGetHistoryLoanPayment",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, get history loan payment not found",
        entity: "loan",
        state: "FailedGetHistoryLoanPayment",
        error: error,
      });
    }
    next();
    return;
  }

  async submitManualPayment(req, res, next) {
    const { loanId } = req.params;
    const transactionTime = new Date();

    const dataPayment = {
      id: uuidv4(),
      loanId: loanId,
      paymentTime: transactionTime,
      paymentMethod: req.body.method || "MANUAL-TRANSFER",
      paymentTo: req.body.destination,
      paymentNominal: req.body.nominal,
      paymentProof: "", //sementara kosong dulu upload bukti transfernya
      paymentStatus: "WAITING-APPROVAL",
    };

    // ada perubahan logic, karena implements approval manual transfer, sehingga tidak langsung impact ke loanBalance user dan status loannya
    const loanPayment = await LoanPayment.create(dataPayment);
    if (loanPayment && loanPayment.id) {
      req.businessLogic = await responseHelper({
        code: 200,
        message: "Submit Manual Payment Loan Success!!",
        entity: "loanPayment",
        state: "SuccessSubmitManualPaymentLoan",
        data: { loanPayment, loan, user },
      });
    } else {
      req.businessLogic = await responseHelper({
        code: 404,
        message: "Sorry, submit manual payment loan failed",
        entity: "loanPayment",
        state: "FailedSubmitManualPaymentLoan",
      });
    }
    next();
    return;
  }

  async rejectManualPayment(req, res, next) {
    const { id } = req.params;
    const adminUID = req.user.uid;

    const dataPayment = {
      verificationStatus: "REJECT",
      verificationTime: new Date(),
      verificationBy: adminUID,
      verificationNotes: req.body.notes || "",
      paymentStatus: "REJECT",
    };

    const loanPayment = await LoanPayment.findByPk(id);
    if (loanPayment && loanPayment.id) {
      await loanPayment.update(dataPayment);
      req.businessLogic = await responseHelper({
        code: 200,
        message: "Reject Manual Payment Loan Success!!",
        entity: "loanPayment",
        state: "SuccessRejectManualPaymentLoan",
        data: loanPayment,
      });
    } else {
      req.businessLogic = await responseHelper({
        code: 404,
        message: "Sorry, reject manual payment loan failed",
        entity: "loanPayment",
        state: "FailedRejectManualPaymentLoan",
      });
    }
    next();
    return;
  }

  async approveManualPayment(req, res, next) {
    const { id } = req.params;
    const loanPayment = await LoanPayment.findByPk(id);
    const adminUID = req.user.uid;

    if (loanPayment && loanPayment.id) {
      const loanId = loanPayment.loanId;
      const loanPaid = await LoanPayment.findOne({
        where: { loanId },
        attributes: [
          "loanId",
          [sequelize.fn("SUM", sequelize.col("paymentNominal")), "totalPaid"],
        ],
        group: ["LoanPayment.loanId"],
        raw: true,
      });

      let paid = {};
      if (loanPaid && loanPaid.loanId) {
        paid = {
          loanId: loanPaid.loanId,
          totalPaid: loanPaid.totalPaid,
        };
      } else {
        paid = {
          totalPaid: 0,
        };
      }

      const loan = await Loan.findByPk(loanId);
      const user = await loan.getCustomer({
        attributes: ["id", "name", "email", "loanBalance"],
      });

      if (loan && loan.id && user && user.id) {
        let transaction = await sequelize.transaction();

        //sesuai bispro terakhir, dimana loan balance hanya dapat digunakan untuk membayar order
        //sehingga tidak perlu diupdate ketika user melakukan payment
        // await user.update(
        //   { loanBalance: +user.loanBalance - +dataPayment.paymentNominal },
        //   { transaction }
        // );

        const balanceWillBe =
          loan.nominal - paid.totalPaid - dataPayment.paymentNominal;

        if (balanceWillBe <= 0) {
          await loan.update(
            {
              totalPaid: +paid.totalPaid + +dataPayment.paymentNominal,
              currentBalance:
                +loan.nominal - +paid.totalPaid - +dataPayment.paymentNominal,
              status: "PAID",
            },
            { transaction }
          );
        } else {
          await loan.update(
            {
              totalPaid: +paid.totalPaid + +dataPayment.paymentNominal,
              currentBalance:
                +loan.nominal - +paid.totalPaid - +dataPayment.paymentNominal,
            },
            { transaction }
          );
        }

        loanPayment.update({
          paymentStatus: "APPROVE",
          verificationStatus: "APPROVE",
          verificationTime: new Date(),
          verificationBy: adminUID,
          verificationNotes: req.body.notes || "",
        });

        await transaction.commit();

        req.businessLogic = await responseHelper({
          code: 200,
          message: "Submit Manual Payment Loan Success!!",
          entity: "loanPayment",
          state: "SuccessSubmitManualPaymentLoan",
          data: { loanPayment, loan },
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: "Sorry, submit manual payment loan failed",
          entity: "loanPayment",
          state: "FailedSubmitManualPaymentLoan",
        });
      }
    } else {
      req.businessLogic = await responseHelper({
        code: 404,
        message: "Sorry, approve manual payment loan failed",
        entity: "loanPayment",
        state: "FailedApproveManualPaymentLoan",
      });
    }
    next();
    return;
  }
}

module.exports = LoanController;
