const responseHelper = require('../../helper/response.helper');
const { User } = require("../../models");
const uploadHelper = require("../../helper/upload.helper")

class userImageController {

    async saveAvatar(req, res, next) {

        try {
            if (!req.body.image) throw "Body image base64 not valid"

            let uplHandler = await uploadHelper.base64(req.body.image)
            if (!uplHandler.success) throw "Failed uploading"

            let image = await uploadHelper.awsStorage({ file: uplHandler.file.image, folder: "avatar" })
            let thumb = await uploadHelper.awsStorage({ file: uplHandler.file.thumb, folder: "avatar", thumb: true })

            await User.update({
                photo: image
            }, { where: { id: req.user.uid } })

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": "Upload Image Success",
                "entity": "userProfile",
                "state": "setUserProfileFailed",
                "data": {
                    photo: image,
                    thumb
                }
            })
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "Upload Image Error",
                "entity": "userProfile",
                "state": "setUserProfileFailed",
                "error": {
                    error
                }
            })
        }
        next()
        return
    }

    async saveIdcard(req, res, next) {

        try {
            if (!req.body.image) throw "Body image base64 not valid"

            let uplHandler = await uploadHelper.base64(req.body.image, false)
            if (!uplHandler.success) throw "Failed uploading"

            let image = await uploadHelper.awsStorage({ file: uplHandler.file.image, folder: "idcard" })

            await User.update({
                identityPhoto: image
            }, { where: { id: req.user.uid } })

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": "Upload Image Success",
                "entity": "userProfile",
                "state": "setUserProfileFailed",
                "data": {
                    photo: image
                }
            })
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "Upload Image Error",
                "entity": "userProfile",
                "state": "setUserProfileFailed",
                "error": {
                    error
                }
            })
        }
        next()
        return
    }



}
module.exports = userImageController