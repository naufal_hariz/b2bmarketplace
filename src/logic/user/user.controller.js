const responseHelper = require('../../helper/response.helper');
const emailHelper = require("../../helper/emailTransaction.helper")
const jwt = require('jsonwebtoken');
const autoBind = require('auto-bind');
//const { v4: uuidv4 } = require('uuid');
const validateInput = require('../../helper/validate.helper');
const bcrypt = require('bcryptjs');

const { User } = require("../../models");
const models = require("../../models");
const userModel = models.User;
const authHelper = require('../../helper/auth.helper');
const { get } = require('https');

class UserController {

    constructor() {
        autoBind(this);
    }

    sendEmail(email, name, req) {
        const token = jwt.sign({
            em: email
        }, process.env.SECRET, { expiresIn: '1h' });

        emailHelper.sendActivateUser({
            to: {
                name: name,
                email: email
            },
            subject: "Tobiko User Activation Link",
            link: req.protocol + "://" + req.headers.host + "/user/activate/" + token,
            template: 2360430
        })
    }

    sendEmailForgot(email, name, req, uuid) {
        const token = jwt.sign({
            string: uuid
        }, process.env.SECRET, { expiresIn: '1h' });
        console.log(uuid)

        emailHelper.sendActivateUser({
            to: {
                name: name,
                email: email
            },
            subject: "Tobiko Reset Password Instruction",
            link: req.protocol + "://" + req.headers.host + "/user/reset/password/" + token,
            template: 2380573
        })
    }

    async register(req, res, next) {

        try {
            let dataUser = req.body
            let { email, name } = req.body

            if (await User.findOne({ where: { email } })) throw "User email already registered"

            dataUser['cityId'] = req.body.city
            dataUser['provinceId'] = req.body.province
            dataUser['password'] = bcrypt.hashSync(req.body.password, 10)
            await User.create(dataUser)

            this.sendEmail(email, name, req)

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Registration success",
                "entity": "Registration",
                "state": "userRegistrationSuccess",
                "data": `Email activation sent to ${email}`
            })
            next()
            return
        } catch (error) {
            console.log(error)

            req.businessLogic = await responseHelper({
                "code": 401,
                "message": "Registration Failed",
                "entity": "Registration",
                "state": "userRegistrationFailed",
                "error": error
            })
            next()
            return

            throw error
        }
    }

    async resend(req, res, next) {
        try {
            let { email } = req.body
            let error, success, checkEmail = await User.findOne({ where: { email } })

            if (checkEmail)
                if (checkEmail.dataValues.isActive === true) {
                    error = "User already activated, please re login"
                } else {
                    success = "Email activation resend success"
                    this.sendEmail(email, checkEmail.dataValues.name, req)
                }
            else error = "Email not registered"

            if (error)
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "message": "Registration Failed",
                    "entity": "Registration",
                    "state": "userRegistrationFailed",
                    "error": error
                })
            else req.businessLogic = await responseHelper({
                "code": 201,
                "message": "Email Registration Success",
                "entity": "Registration",
                "state": "EmailResendRegistrationSuccess",
                "data": success
            })
            next()
            return

        } catch (error) {
            console.log(error)
        }
    }

    async forgot(req, res, next) {
        try {
            let { email } = req.body
            let checkEmail = await User.findOne({ where: { email } })

            if (checkEmail) {

                this.sendEmailForgot(email, checkEmail.name, req, checkEmail.id)

                req.businessLogic = await responseHelper({
                    "code": 201,
                    "message": `Link change password send to email ${email}`,
                    "entity": "ForgotPassword",
                    "state": "EmailForgotPasswordSuccess"
                })
            } else {
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "message": "Forgot Password Failed",
                    "entity": "ForgotPassword",
                    "state": "userRegistrationFailed",
                    "error": "Email not registered"
                })
            }
            next()
            return

        } catch (error) {
            console.log(error)
        }
    }

    async activate(req, res, next) {
        const fs = require('fs')
        try {
            var k = fs.readFileSync('./public/message.html', 'utf8')

            const token = req.params.id
            var decoded = jwt.verify(token, process.env.SECRET);
            let checkEmail = await User.findOne({ where: { email: decoded.em } })

            if (checkEmail.isActive)
                var m = k.replace("{{message}}", "User already active, please relogin from Tobiko App")
            else await User.update({ isActive: true }, { where: { email: decoded.em } })
            var m = k.replace("{{message}}", "User activated, please back to Tobiko app")

            res.send(m);
        } catch (error) {
            console.log(console.log(error))
            var k = fs.readFileSync('./public/message.html', 'utf8')
            var m = k.replace("{{message}}", "Link invalid / expired, please relogin to send link activation from Tobiko app")
            res.send(m);
        }
    }

    async resetForm(req, res, next) {
        const fs = require('fs')
        try {
            const token = req.params.id
            var decoded = jwt.verify(token, process.env.SECRET);

            let checkEmail = await User.findOne({ where: { id: decoded.string } })
            if (!(checkEmail && checkEmail.id)) throw "user not found"
            
            var k = fs.readFileSync('./public/resetForm.html', 'utf8')
            var m = k.replace("{{error}}", "")
            res.send(m);
        } catch (error) {

            var k = fs.readFileSync('./public/message.html', 'utf8')
            var m = k.replace("{{message}}", "Link invalid / expired, please try again to reset")
            res.send(m);
        }

    }

    async resetSave(req, res, next) {
        const fs = require('fs')
        try {
            const token = req.params.id
            var decoded = jwt.verify(token, process.env.SECRET);

            let { password, password2 } = req.body

            var e, frm = fs.readFileSync('./public/resetForm.html', 'utf8')

            if (password === password2 && validateInput.notNull(password)) {
                var msg = fs.readFileSync('./public/message.html', 'utf8')
                var m = msg.replace("{{message}}", "Reset password succeeded, please relogin to Tobiko App")

                await User.update({ password: bcrypt.hashSync(password, 10) }, { where: { id: decoded.string } })

                res.send(m)
            } else if (password !== password2) {
                e = frm.replace("{{error}}", "Please repeat password")
            } else {
                e = frm.replace("{{error}}", "Password not valid")
            }
            res.send(e)
        } catch (error) {
            var k = fs.readFileSync('./public/message.html', 'utf8')
            var m = k.replace("{{message}}", "Link invalid / expired, please try again to reset")
            res.send(m);
        }
    }

    async detailUser(req, res, next) {
        try {
            const getProfile = await authHelper.getUserById(req.user.uid)
            if (getProfile.success) {
                const getPhoto = getProfile.result.photo
                if (getPhoto === null) {
                    req.businessLogic = await responseHelper({
                        code: 200,
                        message: "Get Profile Success",
                        entity: "profile",
                        state: "getProfileSuccess",
                        data: {
                            name: getProfile.result.name,
                            email: getProfile.result.email,
                            phone: getProfile.result.phone,
                            address: getProfile.result.address,
                            provinceId: getProfile.result.provinceId,
                            province: getProfile.result.province,
                            cityId: getProfile.result.cityId,
                            city: getProfile.result.city,
                            photo: null,
                            identityNumber: getProfile.result.identityNumber,
                            identityPhoto: getProfile.result.identityPhoto,
                            identityPhotoSelfie: getProfile.result.identityPhotoSelfie,
                            cashBalance: getProfile.result.cashBalance,
                            loanLimit: getProfile.result.loanLimit,
                            loanBalance: getProfile.result.loanBalance,
                            isActive: getProfile.result.isActive,
                            isAdminB2B: getProfile.result.isAdminB2B,
                            isAdminMerchant: getProfile.result.isAdminMerchant,
                            isCustomer: getProfile.result.isCustomer
                        }
                    })
                } else {
                    req.businessLogic = await responseHelper({
                        code: 200,
                        message: "Get Profile Success",
                        entity: "profile",
                        state: "getProfileSuccess",
                        data: {
                            name: getProfile.result.name,
                            email: getProfile.result.email,
                            phone: getProfile.result.phone,
                            address: getProfile.result.address,
                            provinceId: getProfile.result.provinceId,
                            province: getProfile.result.province,
                            cityId: getProfile.result.cityId,
                            city: getProfile.result.city,
                            photo: getProfile.result.photo,
                            identityNumber: getProfile.result.identityNumber,
                            identityPhoto: getProfile.result.identityPhoto,
                            identityPhotoSelfie: getProfile.result.identityPhotoSelfie,
                            cashBalance: getProfile.result.cashBalance,
                            loanLimit: getProfile.result.loanLimit,
                            loanBalance: getProfile.result.loanBalance,
                            isActive: getProfile.result.isActive,
                            isAdminB2B: getProfile.result.isAdminB2B,
                            isAdminMerchant: getProfile.result.isAdminMerchant,
                            isCustomer: getProfile.result.isCustomer
                        }
                    })
                }
                next()
                return
            } else {
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "message": "Uauthorized",
                    "entity": "profile",
                    "state": "Unauthorized"
                })
                next()
                return
            }
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "profile",
                "state": "getProfileError",
            })
            next()
            return
        }
    }

    async editUser(req, res, next) {
        try {
            const userP = req.user
            const userID = req.user.uid
            const userProfile = await authHelper.getUserById(req.user.uid)
            if (!userProfile) throw "Unauthorized Edit Profile"

            let profileUpdated = {}
            if (req.body.email && req.body.email !== userProfile.email) {
                let checkEmailExist = await userModel.findOne({ where: { email: req.body.email } })
                if (checkEmailExist) {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "api": "Edit Profile",
                        "message": "Email already taken",
                        "entity": "Profile",
                        "state": "editProfileFailed"
                    })
                    next()
                    return
                } else {
                    profileUpdated['email'] = req.body.email
                }
            }

            if (req.body.phone && req.body.phone !== userProfile.phone) {
                let checkPhoneExist = await userModel.findOne({ where: { phone: req.body.phone } })
                if (checkPhoneExist) {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "api": "Edit Profile",
                        "message": "Phone number already taken",
                        "entity": "Profile",
                        "state": "editProfileFailed"
                    })
                    next()
                    return
                } else {
                    profileUpdated['phone'] = req.body.phone
                }
            }

            if (req.body.name) profileUpdated['name'] = req.body.name
            if (req.body.password) profileUpdated['password'] = bcrypt.hashSync(req.body.password, 10)
            if (req.body.address) profileUpdated['address'] = req.body.address
            if (req.body.cityId) profileUpdated['cityId'] = req.body.cityId
            if (req.body.provinceId) profileUpdated['provinceId'] = req.body.provinceId


            if (Object.keys(profileUpdated).length !== 0) {
                await userModel.update(profileUpdated,
                    { where: { id: req.user.uid } })

                if (req.body.password) profileUpdated["password"] = "*****"

                req.businessLogic = await responseHelper({
                    code: 201,
                    message: "Edit Profile Success",
                    entity: "profile",
                    state: "editProfileSuccess",
                    data: profileUpdated
                })
            } else {
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "api": "Update User",
                    "message": "No data change, optional field username, name, password",
                    "entity": "Profile",
                    "state": "editProfileFailed"
                })
            }
            next()
            return

        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 400,
                "entity": "profile",
                "state": "getProfileError",
                "error": error
            })
            next()
            return
        }
    }

}
module.exports = UserController