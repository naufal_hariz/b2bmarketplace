const responseHelper = require('../helper/response.helper');
const { __test_table, Sequelize } = require("../models");

class TestController {

    async list(req, res, next) {
        let data = await __test_table.findAll({ order: [['createdAt', 'ASC']] })
        console.log(data)

        req.businessLogic = await responseHelper({
            "code": 200,
            "message": "Get Review User Success",
            "entity": "Review",
            "state": "getReviewSuccess",
            "data": data
        })
        next()
        return
    }

    async create(req, res, next) {
        try {

            let createdData = await __test_table.create({
                title: req.body.title
            })

            req.businessLogic = await responseHelper({
                code: 200,
                message: "Update success",
                entity: "reviewUser",
                state: "updateReviewUserSUccess",
                data: { createdData }
            })

            next()
            return
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "review",
                "state": "deleteReviewError",
                "error": error
            })
            next()
            return
        }
    }

    async delete(req, res, next) {
        try {

            let id = req.params.id;

            await __test_table.destroy({
                where: { id }
            });

            req.businessLogic = await responseHelper({
                code: 200,
                message: "Update success",
                entity: "reviewUser",
                state: "updateReviewUserSUccess",
                data: id
            })

            next()
            return
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "entity": "review",
                "state": "deleteReviewError",
                "error": error
            })
            next()
            return
        }
    }

}

module.exports = TestController