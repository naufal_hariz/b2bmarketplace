const responseHelper = require("../../helper/response.helper");
const uploadHelper = require("../../helper/upload.helper");
const { ProductGallery } = require("../../models");

class ProductGalleryController {
  async uploadImageProduct(req, res, next) {
    try {
      if (!req.body.image) throw "Body image base64 not valid";

      let uplHandler = await uploadHelper.base64(req.body.image, false);
      if (!uplHandler.success) throw "Failed uploading";

      let image = await uploadHelper.awsStorage({
        file: uplHandler.file.image,
        folder: "product",
      });

      const productGallery = await ProductGallery.create({
        productId: req.body.productId,
        photo: image,
      });

      req.businessLogic = await responseHelper({
        code: 201,
        message: "Upload Photo Product Success",
        entity: "productGallery",
        state: "uploadPhotoProductSuccess",
        data: productGallery,
      });
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Upload Photo Product Error",
        entity: "productGallery",
        state: "uploadPhotoProductFailed",
        error: error,
      });
    }
    next();
    return;
  }
}

module.exports = ProductGalleryController;
