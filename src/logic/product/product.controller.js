const responseHelper = require("../../helper/response.helper");
const {
  Sequelize,
  Product,
  Category,
  ProductGallery,
  Merchant,
} = require("../../models");
const Op = Sequelize.Op;

class ProductController {
  async listProduct(req, res, next) {
    try {
      const limit = 10;
      const page = req.query.page && req.query.page > 0 ? +req.query.page : 1;

      const search = req.query.search ? req.query.search : null;
      const startPrice = req.query.startPrice ? +req.query.startPrice : null;
      const endPrice = req.query.startPrice ? +req.query.endPrice : null;
      const sortBy = req.query.sortBy ? req.query.sortBy : "createdAt";
      const sortDirection = req.query.sortDirection
        ? req.query.sortDirection
        : "DESC";

      const order = [
        [sortBy, sortDirection],
        ["id", "ASC"],
      ];

      const condition = {};
      if (search)
        condition[Op.or] = [
          { name: { [Op.iLike]: "%" + search + "%" } },
          { description: { [Op.iLike]: "%" + search + "%" } },
          { "$category.name$": { [Op.iLike]: "%" + search + "%" } },
        ];
      if (startPrice && endPrice)
        condition.price = { [Op.between]: [startPrice, endPrice] };

      const productCount = await Product.count({
        include: "category",
        where: condition,
      });
      const totalPages = Math.ceil(productCount / limit);

      const products = await Product.findAll({
        include: [
          {
            model: Category,
            as: "category",
            required: true,
          },
          {
            model: Merchant,
            as: "merchants",
            attributes: ["id", "name"],
            through: { attributes: [] },
          },
        ],
        where: condition,
        order,
        limit,
        offset: (page - 1) * limit,
      });

      if (productCount > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve products success!!`,
          entity: "product",
          state: "SuccessGetAllProduct",
          data: {
            page,
            total_pages: totalPages,
            total_data: products.length,
            total_results: productCount,
            products,
          },
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, products not found`,
          entity: "product",
          state: "FailedGetAllProducts",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Get All Products Failed",
        entity: "product",
        state: "FailedGetAllProducts",
        error: error,
      });
    }
    next();
    return;
  }

  async listProductByCategory(req, res, next) {
    try {
      const { categoryId } = req.params;
      const limit = 10;
      const page = req.query.page && req.query.page > 0 ? +req.query.page : 1;

      const search = req.query.search ? req.query.search : null;
      const startPrice = req.query.startPrice ? +req.query.startPrice : null;
      const endPrice = req.query.startPrice ? +req.query.endPrice : null;
      const sortBy = req.query.sortBy ? req.query.sortBy : "createdAt";
      const sortDirection = req.query.sortDirection
        ? req.query.sortDirection
        : "DESC";

      const order = [
        [sortBy, sortDirection],
        ["id", "ASC"],
      ];

      const condition = { categoryId };
      if (search)
        condition[Op.or] = [
          { name: { [Op.iLike]: "%" + search + "%" } },
          { description: { [Op.iLike]: "%" + search + "%" } },
          { "$category.name$": { [Op.iLike]: "%" + search + "%" } },
        ];
      if (startPrice && endPrice)
        condition.price = { [Op.between]: [startPrice, endPrice] };

      const productCount = await Product.count({
        include: "category",
        where: condition,
      });
      const totalPages = Math.ceil(productCount / limit);

      const products = await Product.findAll({
        include: [
          {
            model: Category,
            as: "category",
            required: true,
          },
          {
            model: Merchant,
            as: "merchants",
            attributes: ["id", "name"],
            through: { attributes: [] },
          },
        ],
        where: condition,
        order,
        limit,
        offset: (page - 1) * limit,
      });

      if (productCount > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve products success!!`,
          entity: "product",
          state: "SuccessGetAllProductBasedOnCategory",
          data: {
            page,
            total_pages: totalPages,
            total_data: products.length,
            total_results: productCount,
            products,
          },
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, products not found!`,
          entity: "product",
          state: "FailedGetAllProductsBasedOnCategory",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Get All Products Based on Category Failed",
        entity: "product",
        state: "FailedGetAllProductsBasedOnCategory",
        error: error,
      });
    }
    next();
    return;
  }

  async listProductByFeatured(req, res, next) {
    try {
      const { feature } = req.params;
      const features = [
        "new",
        "recommended",
        "popular",
        "hot",
        "favourite",
        "top",
      ];

      const limit = 10;
      const page = req.query.page && req.query.page > 0 ? +req.query.page : 1;

      const search = req.query.search ? req.query.search : null;
      const startPrice = req.query.startPrice ? +req.query.startPrice : null;
      const endPrice = req.query.startPrice ? +req.query.endPrice : null;
      const sortBy = req.query.sortBy ? req.query.sortBy : "createdAt";
      const sortDirection = req.query.sortDirection
        ? req.query.sortDirection
        : "DESC";

      const order = [
        [sortBy, sortDirection],
        ["id", "ASC"],
      ];

      const condition = {};

      if (search)
        condition[Op.or] = [
          { name: { [Op.iLike]: "%" + search + "%" } },
          { description: { [Op.iLike]: "%" + search + "%" } },
          { "$category.name$": { [Op.iLike]: "%" + search + "%" } },
        ];
      if (startPrice && endPrice)
        condition.price = { [Op.between]: [startPrice, endPrice] };

      if (features.includes(feature)) {
        switch (feature) {
          case "new":
            condition.isNew = true;
            break;
          case "recommended":
            condition.isRecommended = true;
            break;
          case "popular":
            condition.isPopular = true;
            break;
          case "hot":
            condition.isHot = true;
            break;
          case "favourite":
            condition.isFavourite = true;
            break;
          case "top":
            condition.isTop = true;
            break;
        }

        const productCount = await Product.count({
          include: "category",
          where: condition,
        });
        const totalPages = Math.ceil(productCount / limit);

        const products = await Product.findAll({
          include: [
            {
              model: Category,
              as: "category",
              required: true,
            },
            {
              model: Merchant,
              as: "merchants",
              attributes: ["id", "name"],
              through: { attributes: [] },
            },
          ],
          where: condition,
          order,
          limit,
          offset: (page - 1) * limit,
        });

        if (productCount > 0) {
          req.businessLogic = await responseHelper({
            code: 200,
            message: `Retrieve products success!!`,
            entity: "product",
            state: "SuccessGetAllProductBasedOnFeature",
            data: {
              page,
              total_pages: totalPages,
              total_data: products.length,
              total_results: productCount,
              products,
            },
          });
        } else {
          req.businessLogic = await responseHelper({
            code: 404,
            message: `Sorry, products not found!`,
            entity: "product",
            state: "FailedGetAllProductsBasedOnFeatured",
          });
        }
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, feature not defined`,
          entity: "product",
          state: "FailedGetAllProductsBasedOnFeatured",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Get All Products Based on Featured Failed",
        entity: "product",
        state: "FailedGetAllProductsBasedOnFeatured",
        error: error,
      });
    }
    next();
    return;
  }

  async detailProduct(req, res, next) {
    try {
      const { id } = req.params;
      const product = await Product.findByPk(id, {
        include: [
          {
            model: Category,
            as: "category",
            required: true,
          },
          {
            model: Merchant,
            as: "merchants",
            attributes: ["id", "name"],
            through: { attributes: [] },
          },
        ],
      });

      if (product && product.id) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve detail products success!!`,
          entity: "product",
          state: "SuccessGetDetailProduct",
          data: product,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, product with id ${id} not found!`,
          entity: "product",
          state: "FailedGetDetailProducts",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Failed Get Detail Products",
        entity: "product",
        state: "FailedGetDetailProducts",
        error: error,
      });
    }
    next();
    return;
  }

  async latestProduct(req, res, next) {
    try {
      const product = await Product.findAll({
        include: [
          {
            model: Category,
            as: "category",
            required: true,
          },
          {
            model: Merchant,
            as: "merchants",
            attributes: ["id", "name"],
            through: { attributes: [] },
          },
        ],
        limit: 3,
        order: [
          ["createdAt", "DESC"],
          ["id", "ASC"],
        ],
      });

      if (product.length > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve latest products success!!`,
          entity: "product",
          state: "SuccessGetLatestProduct",
          data: product,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, latest product does not exist!`,
          entity: "product",
          state: "FailedGetLatestProducts",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Failed Get Latest Products",
        entity: "product",
        state: "FailedGetLatestProducts",
        error: error,
      });
    }
    next();
    return;
  }

  async uploadSinglePhoto(req, res, next) {
    try {
      const { id } = req.params;
      const product = await Product.findByPk(id);
      if (product && product.id) {
        const uploadSingle = require("../../http/middleware/uploadSingleGalleryProduct.middleware");
        await uploadSingle(req, res);
        const file = req.file;
        const photoProduct = {
          productId: +id,
          photo: `upload/product/${file.filename}`,
        };

        const productGallery = await ProductGallery.create(photoProduct);
        if (productGallery && productGallery.id) {
          req.businessLogic = await responseHelper({
            code: 200,
            message: `Upload single gallery for product id ${id} success`,
            entity: "product",
            state: "SuccessUploadSinglePhotoProduct",
            data: photoProduct,
          });
        } else {
          req.businessLogic = await responseHelper({
            code: 404,
            message: `Upload single photo product failed`,
            entity: "productGallery",
            state: "FailedUploadSinglePhotoProduct",
          });
        }
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Upload single photo product failed due to id product not found`,
          entity: "productGallery",
          state: "FailedUploadSinglePhotoProduct",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Failed Upload Single Photo Products",
        entity: "product",
        state: "FailedUploadSinglePhotoProducts",
        error: error,
      });
    }
    next();
    return;
  }

  async uploadBulkPhoto(req, res, next) {
    try {
      const { id } = req.params;
      const product = await Product.findByPk(id);
      if (product && product.id) {
        const uploadBulk = require("../../http/middleware/uploadBulkGalleryProduct.middleware");
        await uploadBulk(req, res);

        const files = req.files;
        const photoProducts = files.map((file) => {
          return {
            productId: +id,
            photo: `upload/product/${file.filename}`,
          };
        });

        const productGalleries = await ProductGallery.bulkCreate(photoProducts);
        if (productGalleries.length > 0) {
          req.businessLogic = await responseHelper({
            code: 200,
            message: `Upload bulk gallery for product id ${id} success`,
            entity: "product",
            state: "SuccessUploadProductPhotoProduct",
            data: productGalleries,
          });
        } else {
          req.businessLogic = await responseHelper({
            code: 404,
            message: `Upload bulk photo product failed`,
            entity: "productGallery",
            state: "FailedUploadBulkPhotoProduct",
          });
        }
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Upload bulk photo product failed due to id product not found`,
          entity: "productGallery",
          state: "FailedUploadBulkPhotoProduct",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, Failed Upload Bluk Photo Products",
        entity: "product",
        state: "FailedUploadBlukPhotoProducts",
        error: error,
      });
    }
    next();
    return;
  }
}

module.exports = ProductController;
