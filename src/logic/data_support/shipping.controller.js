const responseHelper = require("../../helper/response.helper");
const shippingHelper = require("../../helper/shipping.helper");

class ShippingController {
  async listServices(req, res, next) {
    try {
      const data = req.body;
      const services = await shippingHelper.listServiceDetail(data);
      if (services.length > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve all services shipping success`,
          entity: "shipping",
          state: "SuccessGetAllShippingServices",
          data: services,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, no service shipping available`,
          entity: "shipping",
          state: "FailedGetShippingServices",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "shipping",
        state: "FailedGetShippingServices",
        error: error,
      });
    }
    next();
    return;
  }
}

module.exports = ShippingController;
