const responseHelper = require("../../helper/response.helper");
const { Sequelize, Province, City } = require("../../models");
const Op = Sequelize.Op;

class DistrictController {
  async listDistrict(req, res, next) {
    try {
      const provinces = await Province.findAll({ include: "cities" });

      if (provinces.length > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve all provinces with cities success!!`,
          entity: "province",
          state: "SuccessGetAllProvincesAndCities",
          data: provinces,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, data provinces still empty!!`,
          entity: "province",
          state: "FailedGetAllProvinces",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "province",
        state: "FailedGetAllProvinces",
        error: error,
      });
    }

    next();
    return;
  }

  async listProvince(req, res, next) {
    try {
      const search = req.query.search ? req.query.search : null;
      const provinces = search
        ? await Province.findAll({
            where: { name: { [Op.iLike]: "%" + search + "%" } },
          })
        : await Province.findAll();

      if (provinces.length > 0) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve all provinces success!!`,
          entity: "province",
          state: "SuccessGetAllProvinces",
          data: provinces,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, data provinces does not exist!!`,
          entity: "province",
          state: "FailedGetAllProvinces",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "province",
        state: "FailedGetAllProvinces",
        error: error,
      });
    }
    next();
    return;
  }

  async listCity(req, res, next) {
    try {
      const { id } = req.params;
      const search = req.query.search ? req.query.search : null;
      const province = search
        ? await Province.findByPk(id, {
            include: [
              {
                model: City,
                as: "cities",
                where: { name: { [Op.iLike]: "%" + search + "%" } },
              },
            ],
          })
        : await Province.findByPk(id, { include: "cities" });

      if (province && province.id) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve all cities for Province ${province.name}`,
          entity: "province",
          state: "SuccessGetAllCities",
          data: province,
        });
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message: `Sorry, provinces with id ${id} does not exist!!`,
          entity: "province",
          state: "FailedGetAllCities",
        });
      }
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "cities",
        state: "FailedGetAllCities",
        error: error,
      });
    }
    next();
    return;
  }

  async detailCity(req, res, next) {
    try {
      const { id } = req.params;
      const city = await City.findByPk(id,{include: "province" });

      if (city && city.id) {
        req.businessLogic = await responseHelper({
          code: 200,
          message: `Retrieve city Success`,
          entity: "city",
          state: "SuccessGetCity",
          data: city,
        });
      } else throw "city not found"
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "cities",
        state: "FailedGetAllCities",
        error: error,
      });
    }
    next();
    return;
  }
}

module.exports = DistrictController;
