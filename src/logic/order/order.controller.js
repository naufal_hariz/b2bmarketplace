const responseHelper = require("../../helper/response.helper");
const {
  sequelize,
  Order,
  User,
  Merchant,
  Product,
  OrderWorkflow,
  OrderDetail,
  Workflow,
  Cart
} = require("../../models");
const { v4: uuidv4 } = require("uuid");
const authHelper = require("../../helper/auth.helper");
const invoiceHelper = require("../../helper/invoice.helper");
const emailTransactionHelper = require("../../helper/emailTransaction.helper");

class OrderController {
  async getInvoice(req, res, next) {
    try {
      const { id } = req.params;
      const order = await Order.findByPk(id, {
        include: [
          {
            model: Product,
            as: "products",
            attributes: ["id", "name"],
            through: {
              attributes: ["quantity", "tax", "discount", "total"],
            },
          },
          {
            model: User,
            as: "customer",
            attributes: [
              "id",
              "name",
              "email",
              "phone",
              "address",
              "provinceId",
              "cityId",
            ],
            include: ["province", "city"],
          },
          {
            model: Merchant,
            as: "merchant",
            attributes: [
              "id",
              "name",
              "phone",
              "address",
              "provinceId",
              "cityId",
            ],
            include: ["province", "city"],
          },
          "workflow",
        ],
      });

      const orderDetails = await order.getOrderDetails();
      const totalTransaction = await orderDetails.reduce(
        (total, orderDetail) => {
          return total + orderDetail.total;
        },
        0
      );

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Get Invoice Order Success",
        entity: "order",
        state: "SuccessGetInvoiceOrder",
        data: { order, totalTransaction },
      });
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Sorry, get invoice order failed",
        entity: "invoiceOrder",
        state: "FailedGetInvoiceOrder",
        error: error,
      });
    }
    next();
    return;
  }

  async getTimeline(req, res, next) {
    try {
      const { id } = req.params;
      const order = await Order.findByPk(id, {
        include: [
          {
            model: OrderWorkflow,
            as: "orderWorkflows",
            include: "workflow",
          },
        ],
      });

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Get Timeline Order Success",
        entity: "order",
        state: "SuccessGetTimelineOrder",
        data: { order },
      });
    } catch (error) {
      req.businessLogic = await responseHelper({
        code: 401,
        message: "Get Timeline Order Failed",
        entity: "order",
        state: "FailedGetTimelineOrder",
        error: error,
      });
    }
    next();
    return;
  }

  async paymentUsingLoanBalance(req, res, next) {
    const { id } = req.params;
    const order = await Order.findByPk(id);
    const user = await order.getCustomer({
      attributes: ["id", "name", "email", "loanBalance"],
    });
    const merchant = await order.getMerchant({
      attributes: ["id", "name", "cashBalance"],
    });
    if (order && order.id && user && user.id && merchant && merchant.id) {
      if (order.workflowId === 1) {
        if (user.loanBalance >= order.paymentNominal) {
          let transaction = await sequelize.transaction();
          await user.update(
            { loanBalance: +user.loanBalance - +order.paymentNominal },
            { transaction }
          );

          await merchant.update(
            { cashBalance: +merchant.cashBalance + +order.paymentNominal },
            { transaction }
          );

          const paymentTime = new Date();
          await order.update(
            {
              paymentTime,
              paymentMethod: "LOAN",
              paymentStatus: "PAID",
              workflowId: 2,
            },
            { transaction }
          );

          const dataWorkflow = {
            id: uuidv4(),
            orderId: id,
            workflowId: 2,
            workflowTime: paymentTime,
          };

          const orderWorkflow = await OrderWorkflow.create(dataWorkflow);

          await await transaction.commit();

          req.businessLogic = await responseHelper({
            code: 200,
            message: "Pay Order using Loan Balance Success!!",
            entity: "order",
            state: "SuccessPayOrderUsingLoanBalance",
            data: { order, user, merchant, orderWorkflow },
          });
        } else {
          req.businessLogic = await responseHelper({
            code: 404,
            message:
              "Sorry, submit manual payment loan failed due to insufficient loan balance",
            entity: "loanPayment",
            state: "FailedSubmitManualPaymentLoan",
          });
        }
      } else {
        req.businessLogic = await responseHelper({
          code: 404,
          message:
            "Sorry, submit manual payment loan failed due to order has been paid",
          entity: "loanPayment",
          state: "FailedSubmitManualPaymentLoan",
        });
      }
    } else {
      req.businessLogic = await responseHelper({
        code: 404,
        message:
          "Sorry, submit manual payment loan failed due to user or order not found",
        entity: "loanPayment",
        state: "FailedSubmitManualPaymentLoan",
      });
    }
    next();
    return;
  }

  async submitOrder(req, res, next) {
    try {
      const userId = req.user.uid;
      const { merchantId, total, dataProduct, shipping } = req.body;
      let orderId = "";
      let totalItem = "",
        totalAmount = "";
      let productId = "",
        quantity = "",
        price = "",
        totalPrice = "",
        totalWeight = "";
      let shippingAddress = "",
        shippingPartner = "",
        shippingService = "",
        shippingWeight = "",
        shippingCost = "",
        shippingETD = "";
      let workflowId = 1;

      var d = new Date();
      let invoiceNumber =
        "INV/" +
        String(d.getFullYear()) +
        String(d.getMonth() + 1) +
        String(d.getDate()) +
        "/" +
        String(d.getHours()) +
        String(d.getMinutes()) +
        String(d.getSeconds()) +
        String(d.getMilliseconds());

      for (const property in total) {
        switch (property) {
          case "item":
            totalItem = total[property];
            break;
          case "amount":
            totalAmount = total[property];
            break;
        }
      }

      for (const property in shipping) {
        switch (property) {
          case "address":
            shippingAddress = shipping[property];
            break;
          case "partner":
            shippingPartner = shipping[property];
            break;
          case "service":
            shippingService = shipping[property];
            break;
          case "weight":
            shippingWeight = shipping[property];
            break;
          case "cost":
            shippingCost = shipping[property];
            break;
          case "etd":
            shippingETD = shipping[property];
            break;
        }
      }

      let transaction = await sequelize.transaction();

      const dataSubmitOrder = {
        id: uuidv4(),
        merchantId: merchantId,
        workflowId: workflowId,
        customerId: userId,
        shippingAddress: shippingAddress,
        shippingPartner: shippingPartner,
        shippingService: shippingService,
        shippingWeight: shippingWeight,
        shippingCost: shippingCost,
        shippingETD: shippingETD,
        invoiceNumber: invoiceNumber,
        submittedTime: new Date(),
        totalItem: totalItem,
        totalAmount: totalAmount,
      };
      const order = await Order.create(dataSubmitOrder, { transaction });

      orderId = order["dataValues"]["id"];

      const dataOrderWorkflow = {
        id: uuidv4(),
        orderId: orderId,
        workflowTime: new Date(),
        workflowId: workflowId,
      };
      const orderWorkflow = await OrderWorkflow.create(dataOrderWorkflow, {
        transaction,
      });

      for (var i = 0; i < dataProduct.length; i++) {
        for (const property in dataProduct[i]) {
          switch (property) {
            case "productId":
              productId = dataProduct[i][property];
              break;
            case "quantity":
              quantity = dataProduct[i][property];
              break;
            case "price":
              price = dataProduct[i][property];
              break;
            case "total":
              totalPrice = dataProduct[i][property];
              break;
            case "totalWeight":
              totalWeight = dataProduct[i][property];
              break;
          }
        }

        const dataOrderDetails = {
          id: uuidv4(),
          productId: productId,
          orderId: orderId,
          quantity: quantity,
          price: price,
          total: totalPrice,
          weight: totalWeight,
        };
        const orderDetail = await OrderDetail.create(dataOrderDetails, {
          transaction,
        });

        await Cart.destroy({ where: { userId, productId, merchantId }, transaction })
      }

      await transaction.commit();

      /* send email notif to customer */
      (await invoiceHelper.get(orderId)).sendEmail();

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Submit Order Success",
        entity: "Order",
        state: "SubmitOrder",
        data: order,
      });
    } catch (error) {
      console.log(error);
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "Order",
        state: "SubmitOrder",
        error: error,
      });
    }

    next();
    return;
  }

  async getOrderInfo(req, res, next) {
    try {
      const userId = req.user.uid;

      let data = await Order.findAll({
        attributes: [
          "id",
          "customerId",
          "invoiceNumber",
          "paymentNominal",
          "shippingPartner",
          "shippingService",
          "shippingTrackingNumber",
          "paymentMethod",
          "paymentTo",
          "paymentStatus",
          "createdAt",
          "totalAmount",
        ],
        where: { customerId: userId },
        include: ["customer", "workflow"],
        order: [["createdAt", "DESC"]],
      });

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Get Customer Order Success",
        entity: "CustomerOrder",
        state: "getCustomerOrderSuccess",
        data: data,
      });
    } catch (error) {
      console.log(error);
      req.businessLogic = await responseHelper({
        code: 500,
        message: "Get Customer Order Failed",
        entity: "CustomerOrder",
        state: "getCustomerOrderFailed",
        error: error,
      });
    }

    next();
    return;
  }

  async customerOrderDetail(req, res, next) {
    try {
      const customerId = req.user.uid;
      const orderId = req.params.id;

      let data = await Order.findOne({
        attributes: [
          "id",
          "merchantId",
          "invoiceNumber",
          "paymentNominal",
          "shippingTrackingNumber",
          "totalAmount",
        ],
        where: { customerId, id: orderId },
        include: ["customer", "workflow"],
      });

      let orderDetail = await OrderDetail.findAll({
        where: { orderId },
        include: ["product"],
      });

      req.businessLogic = await responseHelper({
        code: 200,
        message: "Get Order Detail Success",
        entity: "OrderDetail",
        state: "getOrderDetailSuccess",
        data: {
          order: data,
          items: orderDetail,
        },
      });
    } catch (error) {
      console.log(error);
      req.businessLogic = await responseHelper({
        code: 500,
        message: "Get Order Detail Failed",
        entity: "OrderDetail",
        state: "getOrderDetailFailed",
        error: error,
      });
    }

    next();
    return;
  }

  async finishOrder(req, res, next) {
    try {
      const { id } = req.params;
      let workflowId = 5; 

      let transaction = await sequelize.transaction();
      await Order.update({ workflowId }, { where: { id } }, { transaction });
      await OrderWorkflow.create(
        {
          id: uuidv4(),
          orderId: id,
          workflowTime: new Date(),
          workflowId,
        },
        { transaction }
      );
      await transaction.commit();

      let order = await Order.findOne({
        where: { id },
        include: ["customer", "workflow"],
      });
      emailTransactionHelper.sendOrderNotif(order);

      req.businessLogic = await responseHelper({
        code: 200,
        message: "accept order success",
        entity: "orderStatus",
        state: "success",
        data: await OrderWorkflow.findAll({
          where: { orderId: id },
          include: ["workflow"],
        }),
      });
    } catch (error) {
      console.log(error);
      req.businessLogic = await responseHelper({
        code: 401,
        entity: "orderStatus",
        state: "Error",
        message: "order not found",
      });
    }
    next();
    return;
  }
}

module.exports = OrderController;
