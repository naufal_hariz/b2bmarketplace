const responseHelper = require('../../helper/response.helper');
const midtransHelper = require('../../helper/paymentMidtrans.helper');
const { Order, OrderWorkflow, LoanPayment, Loan, User, sequelize, Wallet } = require("../../models");
const uploadHelper = require("../../helper/upload.helper")
const { v4: uuidv4 } = require("uuid");
class PaymentController {

    async trxMidtrans(req, res, next) {
        try {
            let { orderId, total } = req.body,
                order = await Order.findByPk(orderId)
            if (!order) throw { custom: `order id notfound` }

            if (total !== order.totalAmount) throw { custom: `nominal should be ${order.totalAmount}` }

            let payment = await midtransHelper.pay(orderId, total)
            if (!payment.success) throw payment.result

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Midtrans Payment Request Link Success`,
                "entity": "PaymentMidtrans",
                "state": "PaymentRequestSuccess",
                "data": {
                    payment_link: payment.result,
                    orderid: order
                }
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Midtrans Payment Request Link Error`,
                "entity": "PaymentMidtrans",
                "state": "PaymentError",
                "error": (error.custom) ? error.custom : "Midtrans not responding, try another payment option"
            })
            console.log(error)
        }

        next()
        return
    }

    async trxTrasnfer(req, res, next) {
        try {
            let { orderId, total, paymentProof } = req.body,
                order = await Order.findByPk(orderId)

            if (!paymentProof) throw "Body paymentProof base64 not valid"
            if (!order) throw "order id notfound"
            if (total !== order.totalAmount) throw `total should be ${order.totalAmount}`

            let uplHandler = await uploadHelper.base64(paymentProof)
            if (!uplHandler.success) throw "Failed uploading"
            let image = await uploadHelper.awsStorage({ file: uplHandler.file.image, folder: "payment" })

            let param = {
                paymentTime: new Date(),
                paymentMethod: "MANUAL-TRANSFER",
                paymentTo: "MANUAL-TRANSFER",
                paymentNominal: total,
                paymentProof: image,
                paymentStatus: "WAITING-APPROVAL",
                workflowId: 8
            }

            let transaction = await sequelize.transaction();
            await Order.update(param, { where: { id: orderId } }, { transaction })
            await OrderWorkflow.create({ id: uuidv4(), orderId, workflowTime: new Date(), workflowId: 8 }, { transaction })
            await transaction.commit();

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Upload Payment Proof Success, waiting approval admin`,
                "entity": "PaymentTransfer",
                "state": "PaymentTransferSuccess",
                "data": param
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Upload Payment Proof Error, please try again`,
                "entity": "PaymentTransfer",
                "state": "PaymentError",
                error
            })
            console.log(error)
        }
        next()
        return
    }

    async trxManualList(req, res, next) {
        try {
            let payment = await Order.findAll(
                {
                    attributes: ['id', 'invoiceNumber', 'paymentProof', 'paymentNominal', 'paymentTime', 'paymentStatus'],
                    where: { paymentMethod: "MANUAL-TRANSFER" },
                    include: ["customer"],
                    order: [['createdAt', 'DESC']]
                }
            )
            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `List Payment WAITING-APPROVAL success`,
                "entity": "PaymentApproval",
                "state": "PaymentApprovalSuccess",
                "data": payment
            })
        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `List Payment WAITING-APPROVAL failed`,
                "entity": "PaymentApproval",
                "state": "PaymentApprovalFailed",
                error
            })
        }
        next()
        return
    }


    async trxManualApproved(req, res, next) {
        try {
            let { id } = req.params,
                order = await Order.findByPk(id),
                workflowId = 2;

            if (!order) throw "order id notfound"
            if (await OrderWorkflow.findOne({ where: { orderId: id, workflowId } }))
                throw "order already paid"

            let param = { paymentStatus: "APPROVED", workflowId }
            await Order.update(param, { where: { id } })
            await OrderWorkflow.create({ id: uuidv4(), orderId: id, workflowTime: new Date(), workflowId })

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Manual Payment Approved Success`,
                "entity": "PaymentManual",
                "state": "PaymentRequestSuccess",
                "data": {
                    order: await Order.findByPk(id),
                    workflow: await OrderWorkflow.findAll({ where: { orderId: id }, include: ["workflow"] })
                }
            })

        } catch (error) {
            console.log(Error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Manual Payment Approved Error`,
                "entity": "PaymentManual",
                "state": "PaymentError",
                error
            })
        }

        next()
        return
    }

    async trxManualRejected(req, res, next) {
        try {
            let { id } = req.params,
                order = await Order.findByPk(id)

            if (!order) throw "order id notfound"

            let param = { paymentStatus: "REJECT" }
            await Order.update(param, { where: { id } })

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Manual Payment Approved Success`,
                "entity": "PaymentManual",
                "state": "PaymentRequestSuccess",
                "data": await Order.findByPk(id)
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Manual Payment Approved Error`,
                "entity": "PaymentManual",
                "state": "PaymentError",
                error
            })
        }

        next()
        return
    }

    async trxWallet(req, res, next) {
        let transaction
        transaction = await sequelize.transaction();
        try {
            let { orderId } = req.body
            let uid = req.user.uid

            let orderData = await Order.findByPk(orderId)
            let userData = await User.findByPk(uid)

            if (!orderData) throw "order id not found"
            if (!userData) throw "user not found"
            if (orderData["dataValues"]["workflowId"] != 1) throw "order already paid"

            let paymentChange = 0
            let amountToPay = orderData["dataValues"]["totalAmount"];
            let cashBalance = userData["dataValues"]["cashBalance"];

            if (amountToPay > cashBalance) throw "insufficient balance"

            paymentChange = cashBalance - amountToPay


            const paramUser = { cashBalance: paymentChange }
            await User.update(paramUser, { where: { id: uid }, transaction })

            const paramWalletHistory = {
                customerId: uid,
                transactionTime: new Date,
                description: "Payment Order",
                type: "Payment",
                nominal: amountToPay,
            }
            const wallet = await Wallet.create(paramWalletHistory, { transaction })
            let walletId = wallet["dataValues"]["id"];

            const paramOrder = {
                workflowId: 2,
                paymentTime: new Date(),
                paymentMethod: "PAYMENT-VIA-WALLET",
                paymentTo: "PAYMENT-VIA-WALLET",
                paymentNominal: amountToPay,
                paymentProof: walletId,
                paymentStatus: "PAID"
            }
            await Order.update(paramOrder, { where: { id: orderId }, transaction })

            const paramOrderWorkflow = {
                id: uuidv4(),
                orderId: orderId,
                workflowTime: new Date,
                workflowId: 2,
            };
            await OrderWorkflow.create(paramOrderWorkflow, { transaction })

            await transaction.commit();

            orderData = await Order.findByPk(orderId)
            userData = await User.findByPk(uid)

            const data = {
                name: userData["dataValues"]["name"],
                cashBalance: userData["dataValues"]["cashBalance"],
                orderId: orderData["dataValues"]["id"],
                paymentMethod: orderData["dataValues"]["paymentMethod"],
                paymentNominal: orderData["dataValues"]["paymentNominal"],
                paymentStatus: orderData["dataValues"]["paymentStatus"]
            }

            req.businessLogic = await responseHelper({
                code: 200,
                message: "Payment via Wallet Success",
                entity: "payment",
                state: "paymentSuccess",
                data
            })

        } catch (error) {
            // await transaction.rollback()
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Payment via Wallet Error`,
                "entity": "PaymentWallet",
                "state": "PaymentError",
                error
            })
            console.log(error)
        }
        next()
        return
    }

    async loanTrasnfer(req, res, next) {
        try {
            const transactionTime = new Date()

            let { loanId, nominal, paymentProof } = req.body,
                loan = await Loan.findByPk(loanId)

            if (!paymentProof) throw "Body paymentProof base64 not valid"
            if (nominal !== loan.nominal) throw `nominal should be ${loan.nominal}`

            let uplHandler = await uploadHelper.base64(paymentProof)
            if (!uplHandler.success) throw "Failed uploading"
            let image = await uploadHelper.awsStorage({ file: uplHandler.file.image, folder: "payment" })

            const dataPayment = {
                id: uuidv4(),
                loanId: loanId,
                paymentTime: transactionTime,
                paymentMethod: "MANUAL-TRANSFER",
                paymentTo: req.body.destination,
                paymentNominal: nominal,
                paymentProof: image,
                paymentStatus: "WAITING-APPROVAL",
            };

            const loanPayment = await LoanPayment.create(dataPayment);
            if (!(loanPayment && loanPayment.id))
                throw "Loan Payment Failed, loanID not found"

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Upload Loan Payment Proof Success, waiting approval admin`,
                "entity": "LoanPaymentTransfer",
                "state": "LoanPaymentTransferSuccess",
                data: { dataPayment },
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Upload Loan Payment Proof Failed`,
                "entity": "LoanPaymentTransfer",
                "state": "LoanPaymentTransferFailed",
                error
            })
            console.log(error)
        }
        next()
        return
    }

    async trxLoanManualList(req, res, next) {
        try {
            const loanPayments = await LoanPayment.findAll({
                include: [{ association: "loan", include: "customer" }],
                order: [['createdAt', 'DESC']]
            });

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `List Loan Payment WAITING-APPROVAL success`,
                "entity": "LoanPaymentApproval",
                "state": "LoanPaymentApprovalSuccess",
                "data": loanPayments
            })
        } catch (error) {
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `List Loan Payment WAITING-APPROVAL failed`,
                "entity": "LoanPaymentApproval",
                "state": "LoanPaymentApprovalFailed",
                error
            })
        }
        next()
        return
    }

    async trxLoanManualApproved(req, res, next) {
        try {
            let { id } = req.params,
                loanpayment = await LoanPayment.findOne({
                    where: {
                        id,
                        paymentMethod: "MANUAL-TRANSFER",
                        paymentStatus: "WAITING-APPROVAL"
                    }
                })

            if (!loanpayment) throw "loan id notfound"

            let dataLoanPayment = {
                verificationStatus: "APPROVED",
                verificationTime: new Date(),
                verificationBy: req.user.uid

            }

            let transaction = await sequelize.transaction();
            await loanpayment.update(dataLoanPayment, transaction);
            await Loan.update(
                {
                    totalPaid: loanpayment.paymentNominal,
                    currentBalance: 0
                }, { where: { id: loanpayment.loanId } }, { transaction });
            await transaction.commit();

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Manual Loan Payment Approved Success`,
                "entity": "LoanPaymentManual",
                "state": "PaymentRequestSuccess",
                "data": {
                    loan: await LoanPayment.findOne({
                        where: { id },
                        include: [{ association: "loan", include: "customer" }]
                    })
                }
            })

        } catch (error) {
            console.log(Error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Loan Manual Payment Approved Error`,
                "entity": "LoanPaymentManual",
                "state": "LoanPaymentError",
                error
            })
        }

        next()
        return

    }

    async trxLoanManualReject(req, res, next) {
        try {
            let { id } = req.params,
                loanpayment = await LoanPayment.findOne({
                    where: {
                        id,
                        paymentMethod: "MANUAL-TRANSFER",
                        paymentStatus: "WAITING-APPROVAL"
                    }
                })

            if (!loanpayment) throw "loan id notfound"

            let dataLoanPayment = {
                verificationStatus: "REJECT",
                verificationTime: new Date(),
                verificationBy: req.user.uid

            }

            await loanpayment.update(dataLoanPayment);

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Manual Loan Payment Approved Success`,
                "entity": "LoanPaymentManual",
                "state": "PaymentRequestSuccess",
                "data": {
                    loan: await LoanPayment.findOne({
                        where: { id },
                        include: [{ association: "loan", include: "customer" }]
                    })
                }
            })

        } catch (error) {
            console.log(Error)
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Loan Manual Payment Approved Error`,
                "entity": "LoanPaymentManual",
                "state": "LoanPaymentError",
                error
            })
        }

        next()
        return

    }

    async loanMidtrans(req, res, next) {
        try {
            let { loanId, nominal } = req.body,
                loan = await Loan.findByPk(loanId)

            if (!loan) throw { custom: "loan id notfound" }
            if (nominal !== loan.nominal) throw { custom: `nominal should be ${loan.nominal}` }

            let loanpayment = await midtransHelper.pay(loanId, nominal, "loan")
            if (!loanpayment.success) throw loanpayment.result

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Midtrans Payment Request Link Success`,
                "entity": "PaymentMidtrans",
                "state": "PaymentRequestSuccess",
                "data": {
                    payment_link: loanpayment.result,
                    loan
                }
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Midtrans Payment Request Link Error`,
                "entity": "PaymentMidtrans",
                "state": "PaymentError",
                "error": (error.custom) ? error.custom : "Midtrans not responding, try another payment option"
            })
            console.log(error)
        }

        next()
        return
    }

    async loanWallet(req, res, next) {
        let transaction
        try {
            const transactionTime = new Date()
            let { loanId } = req.body
            const uid = req.user.uid
            const paymentMethod = "VIA-WALLET"

            let loanData = await Loan.findByPk(loanId)
            let userData = await User.findByPk(uid)

            if (!loanData) throw "loan id not found"
            if (!userData) throw "user not found"
            if (orderData["dataValues"]["workflowId"] === "PAID") throw "loan already paid"

            let amountToPay = loanData["dataValues"]["nominal"];
            let cashBalance = userData["dataValues"]["cashBalance"];

            if (amountToPay > cashBalance) throw "insufficient balance"

            paymentChange = cashBalance - amountToPay

            transaction = await sequelize.transaction();

            const paramUser = { cashBalance: paymentChange }
            await User.update(paramUser, { where: { id: uid }, transaction })

            const paramLoan = {
                totalPaid: amountToPay,
                status: "PAID",
            };
            const loan = await Loan.update(paramLoan, { where: { id: loanId }, transaction });

            const paramWalletHistory = {
                customerId: uid,
                transactionTime: new Date,
                description: "Payment Loan",
                type: "Payment",
                nominal: amountToPay,
            }
            const wallet = await Wallet.create(paramWalletHistory, { transaction })

            const paramLoanPayment = {
                id: uuidv4(),
                loanId: loanId,
                paymentTime: transactionTime,
                paymentMethod: paymentMethod,
                paymentNominal: amountToPay,
                paymentStatus: "PAID",
            };
            const loanPayment = await LoanPayment.create(paramLoanPayment, { transaction });

            await transaction.commit();

            loanData = await Loan.findByPk(loanId)
            let loanPaymentData = await Loan.findByPk(loanId)

            const data = {
                loadId: loanId,
                paymentTime: loanPaymentData["dataValues"]["paymentTime"],
                paymentNominal: loanData["dataValues"]["totalPaid"],
                paymentMethod: loanPaymentData["dataValues"]["paymentMethod"],
                paymentStatus: loanData["dataValues"]["status"],
            }

            req.businessLogic = await responseHelper({
                "code": 201,
                "message": `Loan Payment by Wallet Success`,
                "entity": "LoanPaymentWallet",
                "state": "PaymentLoanSuccess",
                data
            })

        } catch (error) {
            await transaction.rollback()
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": `Payment by wallet Error`,
                "entity": "PaymentWallet",
                "state": "PaymentError",
                error
            })
            console.log(error)
        }
        next()
        return
    }

}
module.exports = PaymentController