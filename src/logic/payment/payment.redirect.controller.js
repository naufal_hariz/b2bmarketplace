const { Order, OrderWorkflow, sequelize, MidtransPaymentLink, Loan, LoanPayment } = require("../../models");
const { v4: uuidv4 } = require("uuid");
class PaymentRedirectController {

    async finish(req, res, next) {
        const fs = require('fs')
        let { order_id, status_code } = req.query
        var k = fs.readFileSync('./public/message.html', 'utf8')
        var m;

        if (order_id.includes("lx-")) {

            let loanId = await MidtransPaymentLink.findOne({ where: { invoice: order_id } })
            if (loanId) {

                let loan = await Loan.findByPk(loanId.link)

                const dataPayment = {
                    id: uuidv4(),
                    loanId: loanId.link,
                    paymentTime: new Date(),
                    paymentMethod: "MIDTRANS",
                    paymentTo: "MIDTRANS",
                    paymentNominal: loan.nominal,
                    paymentProof: "",
                    paymentStatus: "PAID",
                };

                let transaction = await sequelize.transaction();
                await LoanPayment.create(dataPayment, transaction)
                await Loan.update(
                    {
                        totalPaid: loan.nominal,
                        status: "PAID",
                        currentBalance: 0
                    }, { where: { id: loanId.link } }, { transaction });
                await transaction.commit();

                m = k.replace("{{message}}", "Payment loan via midtrans completed, please back to App")

            } else {
                m = k.replace("{{message}}", "Loan ID not Found")
            }
            res.send(m);

        } else {
            let order = await Order.findOne({ where: { invoiceNumber: order_id, paymentTo: "credit_card", paymentMethod: "MIDTRANS" } })

            if (order && status_code == 200) {
                m = k.replace("{{message}}", "Payment via Credit Card Completed, please back to App")
            } else if (status_code == 200) {
                let transaction = await sequelize.transaction();
                await Order.update(
                    {
                        paymentMethod: "MIDTRANS",
                        paymentTo: "credit_card",
                        paymentTime: new Date(),
                        workflowId: 2
                    },
                    { where: { invoiceNumber: order_id } },
                    { transaction }
                )
                await OrderWorkflow.create({ id: uuidv4(), orderId: order.id, workflowTime: new Date(), workflowId: 2 }, { transaction })
                await transaction.commit();
                m = k.replace("{{message}}", "Payment on process, please check app")
            }
            res.send(m);
        }
    }

    async finishqr(req, res, next) {
        const fs = require('fs')
        let { order_id, status_code } = req.query
        var k = fs.readFileSync('./public/message.html', 'utf8')
        var m;
        let cekQr = await Order.findOne({ where: { invoiceNumber: order_id, paymentTo: "gopay", paymentMethod: "MIDTRANS" } })

        if (cekQr && status_code == 201) {
            m = k.replace("{{message}}", "Payment via QR Gopay Completed, please back to App")
        } else if (status_code == 201) {
            let transaction = await sequelize.transaction();
            await Order.update(
                {
                    paymentMethod: "MIDTRANS",
                    paymentTo: "gopay",
                    paymentTime: new Date(),
                    workflowId: 2,
                    paymentNominal: cekQr.totalAmount
                },
                { where: { invoiceNumber: order_id } },
                { transaction }
            )
            await OrderWorkflow.create({ id: uuidv4(), orderId: id, workflowTime: new Date(), workflowId: 2 }, { transaction })
            await transaction.commit();
            m = k.replace("{{message}}", "Payment on process, please check app")
        }
        res.send(m);
    }

    async notify(req, res, next) {

        try {
            if (req.body.headers.host !== "tobiko.dzahin.com")
                throw "Request not found"

            let { payment_type, order_id, gross_amount, transaction_status } = req.body.body
            gross_amount = parseInt(gross_amount)

            let gopay = (payment_type === "gopay" && transaction_status === "pending")
            let cc = (payment_type === "credit_card" && transaction_status === "capture")

            if (gopay || cc) {

                if (order_id.includes("lx-")) {
                    let loanId = await MidtransPaymentLink.findOne({ where: { invoice: order_id } })
                    if (loanId) {

                        let loan = await Loan.findByPk(loanId.link)
                        const dataPayment = {
                            id: uuidv4(),
                            loanId: loanId.link,
                            paymentTime: new Date(),
                            paymentMethod: "MIDTRANS",
                            paymentTo: "gopay",
                            paymentNominal: loan.nominal,
                            paymentProof: "",
                            paymentStatus: "PAID",
                        };

                        let transaction = await sequelize.transaction();
                        await LoanPayment.create(dataPayment, transaction)
                        await Loan.update(
                            {
                                totalPaid: loan.nominal,
                                status: "PAID",
                                currentBalance: 0
                            }, { where: { id: loanId.link } }, { transaction });
                        await transaction.commit();

                    }
                } else {
                    let updatedData = await Order.findOne({ where: { invoiceNumber: order_id } })

                    let transaction = await sequelize.transaction();
                    await Order.update(
                        {
                            paymentMethod: "MIDTRANS",
                            paymentTo: payment_type,
                            paymentNominal: gross_amount,
                            paymentTime: new Date(),
                            workflowId: 2
                        },
                        { where: { invoiceNumber: order_id } },
                        { transaction }
                    )
                    await OrderWorkflow.create({ id: uuidv4(), orderId: updatedData.id, workflowTime: new Date(), workflowId: 2 }, { transaction })
                    await transaction.commit();
                }
                res.send("ok")
            }
        } catch (error) {
            res.send(error)
        }
    }
}
module.exports = PaymentRedirectController