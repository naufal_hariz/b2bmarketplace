const { RAJAONGKIR, APIKEYRAJAONGKIR } = require("../api/api");
const querystring = require("querystring");

class RajaOngkirRepository {
  async getListProvinceFromRajaOngkir() {
    const response = await RAJAONGKIR.get("/province", {
      headers: {
        key: APIKEYRAJAONGKIR,
      },
    });
    return response.data.rajaongkir;
  }

  async getListCitiesFromRajaOngkir() {
    const response = await RAJAONGKIR.get("/city", {
      headers: {
        key: APIKEYRAJAONGKIR,
      },
    });
    return response.data.rajaongkir;
  }

  async postBrowseServices(data) {
    try {
      const qs = querystring.stringify(data);
      const response = await RAJAONGKIR.post("/cost", qs, {
        headers: {
          key: APIKEYRAJAONGKIR,
        },
      });
      const services = response.data.rajaongkir.results[0].costs.map(
        (service) => {
          return {
            name: service.service,
            description: service.description,
            cost: service.cost[0].value,
            etd: service.cost[0].etd,
          };
        }
      );
      return services;
      // return result jika ingin informasinya lebih detail, namun pengecekannya di controllernya harus disesuaikan karena result tipenya object, bukan array
      // const result = {
      //   origin: response.data.rajaongkir.origin_details,
      //   destination: response.data.rajaongkir.destination_details,
      //   partner: {
      //     code: response.data.rajaongkir.results[0].code,
      //     name: response.data.rajaongkir.results[0].name,
      //   },
      //   services,
      // };
      // return results;
    } catch (error) {
      //terjadi error apa pun, returnkan services dummy ini, supaya aplikasi tetap jalan.. wkwkwk
      const servicesDummy = [
        {
          name: "OKE",
          description: "Economy Service",
          cost: Math.floor(Math.random() * 6 + 10) * 1000,
          etd: "4",
        },
        {
          name: "REG",
          description: "Regular Service",
          cost: Math.floor(Math.random() * 6 + 16) * 1000,
          etd: "2",
        },
        {
          name: "ONS",
          description: "One Night Service",
          cost: Math.floor(Math.random() * 6 + 21) * 1000,
          etd: "1",
        },
      ];
      return servicesDummy;
    }
  }
}

module.exports = RajaOngkirRepository;
