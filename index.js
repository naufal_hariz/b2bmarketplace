const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const routes = require("./src/http/route/rout‎e");
const publicRoutes = require("./src/http/route/publicRoute");
const CORSMiddleware = require("./src/http/middleware/CORS.middleware");
const responseMiddleware = require("./src/http/middleware/response.middleware");
const logMiddleware = require("./src/http/middleware/log.middleware");
const path = require("path");
global.baseDir = path.resolve(__dirname);

const env = require("dotenv");
env.config();

app.use(express.static("public"));
app.use(bodyParser.json({extended: true,
  limit: '2mb'}));
app.use(CORSMiddleware);
app.set("json spaces", 2);
app.use("/api", routes);
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", publicRoutes);

app.use(logMiddleware);
app.use(responseMiddleware);

app.listen(process.env.PORT, () => {
  console.log(
    `Server is listening on port ${process.env.PORT} with Application Name: ${process.env.APPLICATION_NAME}`
  );
});
