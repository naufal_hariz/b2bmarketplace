## Team E Backend Mini Project

This project is used to Backend App for Use Case APP B2B MarketPlace
Telkomsel Softdev Academy 2021

## Team E Members

1. Hario Baskoro Basoeki (BE)
2. Agus Dwi Prayogo (D) (BE)
3. Revan Faredha (A) (BE)
4. Sugeng Dinda Winanjuar (BE)
5. I Wayan Gede Suarda (A) (BE)
6. Ayrtein Deliusca Dian Pratama (D) (BE)
7. Yudi Marta Arianto (A) (AND)
8. Irfan Saputra (D) (AND)
9. Raditya (A) (iOS)
10. Panji G Prasetya (D) (iOS)

## API Documentation



## POSTMAN Collection


## POSTMAN Request Capture


## Database Relation


### Cheers! 🍻
Team E - Softdev Telkomsel